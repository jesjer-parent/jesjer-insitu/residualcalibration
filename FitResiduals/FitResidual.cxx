#define NEWRESIDUAL_H
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>

#include "TCut.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TList.h"
#include "TEventList.h"
#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TSystem.h"
#include "TColor.h"
#include "TChain.h"
#include "TLegend.h"
#include "TF2.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TH3F.h"
#include "TApplication.h"
#include "../macros/CalibTools.h"
#include "TStyle.h"
#include "TEnv.h"
#include "../macros/ResidualTools.h"

#define NBINS 34

std::vector<double> etaFitBins; //important to include the upper bound bin
Double_t etaDependence(Double_t* x, Double_t* par)
{
  float xx = x[0];
  double par0new = par[0];
  for(unsigned int i = 1; i < etaFitBins.size()-1; ++i){  
    if(xx < etaFitBins.at(i)) return par0new + par[i]*(xx-etaFitBins.at(i-1));
    par0new = par0new + par[i]*(etaFitBins.at(i) - etaFitBins.at(i-1));
  }
  return par0new + par[etaFitBins.size()-1]*(xx - etaFitBins.at(etaFitBins.size()-2));
}

int main(int argc, char * argv[]){

  //Reading file TH3 from inputfile
  TString InputFile  (argv[1]);
  std::cout<<"InputFile: "<<InputFile<<std::endl;
  TFile* file = TFile::Open(InputFile);
  if(!file){
    std::cout << InputFile << " not found, exiting" << std::endl;
    return 0;
  }
  //Reading  List from file
  TList *list = (TList*)file->Get("DeltaPtHistList");

  if(!list){
    std::cout <<"List not found, exiting" << std::endl;
    return 0;
  }
  std::cout<<"list read"<<std::endl;


  // Open config
  TString configName (argv[2]);
  TEnv* config = new TEnv(configName);
  if(!config){
    std::cout << configName << " not found, exiting" << std::endl;
    return 0;
  }
  std::cout<<"Config File"<<configName<<std::endl;
  config->Print();

    //Creating auxiliar list needed to run the functions from previous code
  TList* auxlist = new TList();
  int nptbins = config->GetValue("TruePtNBin",1);
  int netabins = config->GetValue("TrueEtaNBin",1);
  for(int etacount = 0; etacount<netabins; ++etacount){
    TList* auxlist2 = new TList(); 
    for(int ptcount = 0; ptcount<nptbins; ++ptcount){
      auxlist2->Add((TList*)list->At(etacount*nptbins+ptcount));
    }
    auxlist->Add(auxlist2);
  }
  std::cout<<"Created auxiliar list"<<std::endl;
  auxlist->Print();
 // Get term (mu or npv)
  TString Term(argv[3]); 
  std::cout<<"Term: "<<Term<<std::endl;
  
  
  // TFile fits("fittings.root","RECREATE"); //first test
  TList* h_slice_list =NULL;
  if(Term.Contains("Mu")){
    h_slice_list = PTools::Slice3D(auxlist, "zx",2);  
  }
  else if (Term.Contains("NPV")){
    h_slice_list = PTools::Slice3D(auxlist, "zy",2);  
  }
  TList* h_mean_list = PTools::GetMean(h_slice_list,3);

  // Read Fit Function and Range
  TString Fit = config->GetValue("Fit","[0]+[1]*x");
  std::cout<<"Fit: "<<Fit<<std::endl;
  float FitHigh = 0.;
  float FitLow = 0.;
  if(Term=="MuTerm"){
    FitLow = config->GetValue("FitLowMu",5.);
    FitHigh = config->GetValue("FitHighMu",30.);
  } else if(Term=="NPVTerm"){
    FitLow = config->GetValue("FitLowNPV",5.);
    //    FitHigh = config->GetValue("FitHighPNV",30);
    FitHigh = config->GetValue("FitHighNPV",30);
  }
  std::cout<<"FitLow: "<<FitLow<<std::endl;
  std::cout<<"FitHigh: "<<FitHigh<<std::endl;

  std::cout<<"initialize TF1 'fit'"<<std::endl;
  TF1* fit = new TF1("fit",Fit,FitLow,FitHigh);
  fit->SetParameter(0,0.); fit->SetParameter(1,0.); //Seed values
    
  std::cout<<"Building h_fit_mean_list"<<std::endl;
  TList* h_fit_mean_list = PTools::Fit(h_mean_list,fit,3,"R");

  std::vector<float> HPar1Vec;
  int HPar1NBin   = 0;
  float HPar1Low  = 0.;
  float HPar1High = 0.;
  if(Term=="MuTerm"){
    HPar1NBin = config->GetValue("NPVNBin",25);
    HPar1Low  = config->GetValue("NPVLow", 9.5);
    HPar1High = config->GetValue("NPVHigh",34.5);
  } else if(Term=="NPVTerm"){
    HPar1NBin = config->GetValue("muNBin",25);
    HPar1Low  = config->GetValue("muLow", 9.5);
    HPar1High = config->GetValue("muHigh",34.5);
  }
  std::cout<<"HPar1NBin: "<<HPar1NBin<<std::endl;
  std::cout<<"HPar1Low: "<<HPar1Low<<std::endl;
  std::cout<<"HPar1High: "<<HPar1High<<std::endl;
  float interval = (HPar1High - HPar1Low)/((float)HPar1NBin); 
  std::cout<<"interval =  (HPar1High - HPar1Low)/((float)HPar1NBin) = "<<interval<<std::endl;
  for(int i = 0; i < HPar1NBin; ++i){         //Looping over NPV or mu
    HPar1Vec.push_back(HPar1Low+(interval*(float)i));
  }
  HPar1Vec.push_back(HPar1High);
  std::cout<<"HPar1Vec: ";
  for(unsigned int i = 0 ; i < HPar1Vec.size(); ++i)std::cout<<HPar1Vec.at(i)<<" ";
  std::cout<<std::endl;
  
  std::cout<<"Building par_h_fit_mean_list"<<std::endl;    //Creating a list of (eta,pt) histograms  with the result of the fit (par1 is the slope of the linear function) for each mu(NPV)
  TList* par_h_fit_mean_list = PTools::PlotMultiFitParams(h_fit_mean_list,HPar1Vec,2,2);


  TString Average = config->GetValue("Average","[0]"); std::cout<<"Average: "<<Average<<std::endl;
  float AverageLow  = 0.;
  float AverageHigh = 0.;
  if(Term=="MuTerm"){
    AverageLow = config->GetValue("AverageLowNPV",10.5);
    AverageHigh = config->GetValue("AverageHighNPV",34.5);
  } else if(Term=="NPVTerm"){
    AverageLow = config->GetValue("AverageLowMu",10.5);
    AverageHigh = config->GetValue("AverageHighMu",34.5);
  }
  std::cout<<"AverageLow: "<<AverageLow<<std::endl;
  std::cout<<"AverageHigh: "<<AverageHigh<<std::endl;


  std::cout<<"initialize TF1 'scalar'"<<std::endl;
  TF1* scalar = new TF1("scalar",Average,AverageLow,AverageHigh);;
  scalar->SetParameter(0,0.);

  std::cout<<"building list 'average'"<<std::endl;
  TList* average = PTools::Fit((TList*)par_h_fit_mean_list->At(1),scalar,2,"R"); //Fitting each of the previous histograms with a scalar

  TString etavariable("TrueEtaBin");
  std:: vector<float> etabins=ReadBinning(config,etavariable,netabins);
  std::cout<<"building eta dependence plots"<<std::endl;
  TList* EtaDependencePlots = PlotEtaDependence(average, etabins);

  std::cout<<"building list 'par_average'"<<std::endl;
//  int nptbins = config->GetValue("TruePtNBin",1);
  TString ptvariable("TruePtBin");
  std::vector<float> ptbins=ReadBinning(config,ptvariable,nptbins);
  TList* par_average = PTools::PlotMultiFitParams(average,ptbins,1,1); //Generating histograms of the averaged over mu(NPV) dependence on NPV(mu) as function os pTtrue for each eta bin

  // par_average->Print();
  
  new TCanvas;
  std::cout<<"initialize TF1 'TruePtFit'"<<std::endl;
  double minpTtrue = config->GetValue("TruePtFitLow",5.0);
  double maxpTtrue = config->GetValue("TruePtFitHigh",200.0);
  TString sIPar0Fit = config->GetValue("TruePtFit","[0]+[1]*log(x/25.0)");
  TF1* TruePtFit = new TF1("TruePtFit",sIPar0Fit,minpTtrue,maxpTtrue);
  TruePtFit->SetParameter(0,0.); TruePtFit->SetParameter(1,1.);
 
  std::cout<<"building list fit_TruePtFit_list"<<std::endl;

  TList* fit_TruePtFit_list = PTools::Fit((TList*)par_average->At(0), TruePtFit, 1);  //Fitting the pttrue  dependence with a logarithmic function for each bin of eta --- number of histograms equal to number of eta bins. [0] parameter is the dependence at ptTrue = 25 GeV

  std::cout<<"building list par_fit_TruePtFit_list"<<std::endl;
  TList* par_fit_TruePtFit_list = PTools::PlotFitParams(fit_TruePtFit_list, etabins, 2);  //Producing 2 histograms  of the eta dependence of the parameters from the logarithmic fit. [0] parameter is the dependence at ptTrue = 25 GeV

  std::cout<<"initialize TF1 'TrueEtaFit'"<<std::endl;
 // Get eta fit bins
  etaFitBins = VectorizeD(config->GetValue("ResidualAbsEtaBins","0 0.9 1.2 1.5 1.8 2.4 2.8 3.2 3.5 4.0 4.3 6.0"));
  TF1* TrueEtaFit = new TF1("TrueEtaFit", etaDependence, 0., 5., etaFitBins.size());
  TrueEtaFit->SetParameters(-0.05, 0., 0., 0., 0.);
  TFitResultPtr fr0;
  TFitResultPtr fr1; 
  std::cout<<"initialize TF1's 'fit0_TrueEtaFit' and 'fit1_TrueEtaFit' "<<std::endl;
  TF1* fit0_TrueEtaFit = PTools::Fit((TH1*)par_fit_TruePtFit_list->At(0), TrueEtaFit, etaDependence, fr0);
  if(!fit0_TrueEtaFit) std::cout << "fit0_TrueEtaFit does not exist" << std::endl;
  TF1* fit1_TrueEtaFit = PTools::Fit((TH1*)par_fit_TruePtFit_list->At(1), TrueEtaFit, etaDependence, fr1);
  if(!fit1_TrueEtaFit) std::cout << "fit1_TrueEtaFit does not exist" << std::endl;
  
  std::cout<<"initialize TH1D's 'h_uncfit' and 'h_uncfitP' "<<std::endl;
  TH1* h_uncfit = new TH1D("uncfit","uncfit",NBINS,((TH1*)par_fit_TruePtFit_list->At(0))->GetXaxis()->GetXmin(),((TH1*)par_fit_TruePtFit_list->At(0))->GetXaxis()->GetXmax());
  std::cout << "after creating h_uncfit" << std::endl;
  TH1* h_uncfitP = new TH1D("uncfitP","uncfitP",NBINS,((TH1*)par_fit_TruePtFit_list->At(0))->GetXaxis()->GetXmin(),((TH1*)par_fit_TruePtFit_list->At(0))->GetXaxis()->GetXmax());

  double xxx[NBINS];
  for(int i = 0; i < NBINS; ++i){
    xxx[i] = h_uncfit->GetBinCenter(i);
  }

  double err[NBINS];  // error on the function at point x0
  double errP[NBINS];

  fr0->GetConfidenceIntervals(NBINS, 1, 1, xxx, err, 0.683, false);
  fr0->GetConfidenceIntervals(NBINS, 1, 1, xxx, errP, 0.683, true);

  for(int j = 1; j <= NBINS; ++j){
    h_uncfit->SetBinContent(j,fit0_TrueEtaFit->Eval(h_uncfit->GetBinCenter(j)));
    h_uncfit->SetBinError(j,err[j-1]);
    h_uncfitP->SetBinContent(j,fit0_TrueEtaFit->Eval(h_uncfitP->GetBinCenter(j)));
    h_uncfitP->SetBinError(j,errP[j-1]);
  }

  std::vector<double> fitVector;
  std::cout<<"fitVector: ";
  for(unsigned int j = 0; j < etaFitBins.size(); ++j){
    fitVector.push_back(fit0_TrueEtaFit->GetParameter(j));
    std::cout<<fitVector.at(j)<<" ";
  }
  std::cout<<std::endl;


  // Get path
  TString path = gSystem->ExpandPathName("$PWD");

  // Get version
  TString ResidualVersion = config->GetValue("Version","VERSION");

  //Get jet algo
  TString JetAlgo = config->GetValue("JetAlgos","");
  //Create directory for results
  system("mkdir residual");
  std::cout<<"writing residual config results to file: "<<path<<"/residual/"<<Term<<ResidualVersion<<JetAlgo<<".config"<<std::endl;
  TEnv* outResidualE = new TEnv(path+"/residual/"+Term+ResidualVersion+JetAlgo+".config");
  
  if(Term.Contains("NPV")) outResidualE->SetValue(ResidualVersion+JetAlgo+".Description", ResidualVersion+" mu- and N_{PV}-dependent jet pileup corrections");
  if(Term.Contains("NPV")) outResidualE->SetValue(ResidualVersion+JetAlgo+".AbsEtaBins",Devectorize(etaFitBins));
  outResidualE->SetValue(ResidualVersion+JetAlgo+"."+Term+"."+JetAlgo,Devectorize(fitVector));
  outResidualE->SaveLevel(kEnvLocal);
  
  std::cout<<"writing data, fits, histos to file: "<<path<<"/residual/"<<Term<<ResidualVersion<<JetAlgo<<".root"<<std::endl;
  PTools::Pause(path+"/residual/"+Term+ResidualVersion+JetAlgo+".root");

  return 0;
}

//g++ -o FitResidual FitResidual.cxx `root-config --cflags --glibs`
