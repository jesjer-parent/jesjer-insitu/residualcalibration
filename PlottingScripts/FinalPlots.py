#!/usr/bin/python
import os, sys
from ROOT import *

# PATH to ROOT files
PATH             = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/EMTopo_extendedranges_AfterPileUp"
#PATH             = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/EMTopo_extendedranges_AfterGSC"
#PATH             = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/EMTopo_extendedranges"
#PATH             = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/EMTopo_extendedranges_weight"
#PATH             = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/EMTopo_extendedranges_mcEventWeight"
#PATH             = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/EMTopo_previousranges"
PATHtoAtlasStyle = "/nfs/dust/atlas/user/rivadene/qualification/atlasrootstyle"
Version          = "AfterPileupResidual_201118" # Same one used in config file
#Version          = "AfterGSC_201118" # Same one used in config file
#Version          = "22102018" # Same one used in config file
JetCollection    = "AntiKt4EMTopoJets"

Terms = [
  "Mu",
  "NPV",
]

# Compare with calibration in a config file (example MC16a_Residual_4EM_4LC.config)
#CalibConfig = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16a_Residual_4EM.config"
CalibConfig = "NONE"

##################################################################################
# DO NOT MODIFY
##################################################################################

PATH += "/"
PATHtoAtlasStyle += "/"
gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro(PATHtoAtlasStyle+"AtlasStyle.C")
SetAtlasStyle()
gROOT.ProcessLine(".L loader.C+") # So I can read vector<vector<float> > branches

def parseline(ln):
    ls = list()
    desc = True 
    for w in ln.split():
        if desc and ':' in w:
            desc = False
            continue
        if not desc:
            ls.append(float(w))
    return ls

class linpiece_func:
  
  """
      class of linear piecewise functions (callable) constructed from the residual pielup calibration files.
      One instance oorresponds to a single calibration curve.
  """

  def __init__(self,calib,algo = 'AntiKt2',term = 'NPV'):
    self.algo = algo #for debuggingand testing
    self.term = term #for debuggingand testing
    self.slope = list() # calibration slope parameter values in som eAbsEta range
    self.etalo = list() # abseta bin lower edges of the calibration
    self.offs = list() # the value of the function at eta = etalo[i]
    # parse the file  line by line and find slope and etalo in there
    with open(calib,'r') as f:
      for l in f:
        if algo in l and term in l:
          if self.slope:
            print "Warning: Found more than one "  + term + " - term  calibration for " + algo + " jets in file  '" + calib + "'."
            print "Overwriting previously found calibration."
            self.slope = list()
          self.slope = parseline(l)
        elif 'AbsEtaBins' in l:
          if self.etalo:
            print "Warning: Found more than one set of AbsEtaBins in file '" + calib + "'."
            print "Overwriting previously found AbsEtaBins."
            self.etalo = list()
          self.etalo = parseline(l)
      if (not self.etalo) or (not self.slope):
        raise IOError('failed to parse ' + calib)
      # calculate offsets once when initailising, then hold in memory
      # do not recalcualte with every __call__
      self.offs.append(self.slope[0])
      for i in range(1,len(self.etalo)):
        es = self.etalo[i] - self.etalo[i-1]
        no = self.offs[i-1] + self.slope[i]*es
        self.offs.append(no)
      f.close()

  def __call__(self,x):
    y = float('nan')
    x = x[0]
    for i in range(len(self.etalo) -1):
      if x >= self.etalo[i] and x <self.etalo[i+1]:
        y = self.offs[i] + (x - self.etalo[i])*self.slope[i+1] 
        break
    return y

MuHistName = "eta_0_pt_0_zx0_mean_myfit_par1_myfit_par0_myfit_par0"
BandName = "uncfitP"
NPVHistName = "eta_0_pt_0_zy0_mean_myfit_par1_myfit_par0_myfit_par0"

# Loop over terms
for term in Terms:
  # Open file
  FileName = PATH+term+"Term"+Version+JetCollection+".root"
  File     = TFile.Open(FileName,"READ")
  if not File:
    print FileName+" not found, exiting"
    sys.exit(0)
  # Get Histogram
  if term == "Mu":
    Hist = File.Get(MuHistName)
    if not Hist:
      print MuHistName+" not found, exiting"
      sys.exit(0)
  elif term == "NPV":
    Hist = File.Get(NPVHistName)
    if not Hist:
      print NPVHistName+" not found, exiting"
      sys.exit(0)  
  Hist.GetXaxis().SetTitle("|#eta|")
  if term == "Mu":
    Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial#mu [GeV]")
    Hist.SetMinimum(-0.35)
  elif term=="NPV":
    Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial N_{PV} [GeV]")
    Hist.SetMaximum(0.4)
  gStyle.SetErrorX(.5)
  gStyle.SetOptStat(0)
  Hist.SetLineColor(kBlue -2)
  Hist.SetMarkerColor(kBlue -2)
  Hist.SetMarkerSize(0.5)
  # Get Fit Error
  Band = File.Get(BandName)
  Band.SetMarkerSize(0.)
  Band.SetFillColorAlpha(kRed,0.35)
  # Get Fit
  if term == "Mu":
    Hist.GetFunction(MuHistName+"_myfit").SetLineColor(kRed)
    Hist.GetFunction(MuHistName+"_myfit").SetMarkerColor(kRed)
  elif term == "NPV":
    Hist.GetFunction(NPVHistName+"_myfit").SetLineColor(kRed)
    Hist.GetFunction(NPVHistName+"_myfit").SetMarkerColor(kRed)
  # Compare with calibration in config file (if requested)
  if "NONE" not in CalibConfig:
    compf = linpiece_func(CalibConfig,"AntiKt4EMTopo",term)
    tf1 = TF1('compf',compf,Hist.GetXaxis().GetXmin(),Hist.GetXaxis().GetXmax())
    tf1.SetLineColor(kGreen+2)
    tf1.SetTitle(compf.algo)
  # TCanvas
  Can = TCanvas()
  # Name of output PDF
  outPDF  = PATH
  outPDF += term
  outPDF += "Term.pdf"
  Can.Print(outPDF+"[")
  Hist.Draw("ERR")
  Band.Draw("E3 SAME")
  if "NONE" not in CalibConfig:
    tf1.Draw('same l')
  Can.Print(outPDF)
  Can.Print(outPDF+"]")
print ">>> ALL DONE <<<"
