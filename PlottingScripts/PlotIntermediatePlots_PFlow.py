from ROOT import *
import os,sys

# PATH to ROOT files
PATH = "/nfs/dust/atlas/user/rivadene/qualification/residualcalibration/FitResiduals/cppresidual/residual/PFlow_extendedrangesv2"

Terms = [
  "Mu",
  "NPV",
]

# PATH to AtlasStyle.C
PATHtoAtlasStyle = "/nfs/dust/atlas/user/rivadene/qualification/atlasrootstyle"

# Version used in config file
Version = "191218"
#Version = "AfterPileupResidual_191218"
#Version = "AfterGSC_191218"

JetCollection = "AntiKt4EMPFlowJets"

FineEtaBinning = True # if False: Coarse Eta Binning
#FineEtaBinning = False 
nMuBins  = 18
nNPVBins = 18

#################################################
## DO NOT MODIFY
#################################################

PATH += "/"
PATHtoAtlasStyle += "/"
gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro(PATHtoAtlasStyle+"AtlasStyle.C")
gROOT.ProcessLine(".L loader.C+") # So I can read vector<vector<float> > branches
gStyle.SetOptStat(0)
SetAtlasStyle()

nEtaBins = 41
if FineEtaBinning:
  EtaBins = ["0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0","2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0","3.1","3.2","3.3","3.4","3.5","3.6","3.8","4.0","4.2","4.5","4.9"]
else:
  nEtaBins = 10	
#  EtaBins = ["0.0","0.2","0.4","0.6","0.8","1.0","1.2","1.4","1.6","1.8","2.0","2.2","2.4","2.6","2.8","3.0","3.2","3.4","3.6","4.0","4.5","4.9"]
  EtaBins = ["0.0","0.3","0.8","1.2","1.6","2.1","2.4","2.8","3.2","3.6","4.5"]

#################################
# pTresidual plots
# Loop over terms
for term in Terms:
  # TCanvas	
  can = TCanvas()
  # Name of output PDF
  outPDF = PATH+"pTresidualVS"+term+".pdf"
  can.Print(outPDF+"[")
  # Open file
  FileName = PATH + term + "Term" + Version + JetCollection + ".root"
  File     = TFile.Open(FileName,"READ")
  if not File:
    print FileName + " not found, exiting"
    sys.exit(0)
  # Loop over eta bins
  for ieta in range(0,nEtaBins):
    # Loop over mu/npv bins
    nBins = nMuBins
    if term=="NPV":
      nBins = nNPVBins
    for ibin in range(0,nBins):
      # Get TH1D
      HistName  = "eta_"
      HistName += str(ieta)
      if term == "Mu":
        HistName += "_pt_0_zx"
      elif term == "NPV":
        HistName += "_pt_0_zy"
      HistName += str(ibin)
      HistName += "_mean"
      Hist = File.Get(HistName)
      if not Hist:
        print HistName + " not found, exiting"	      
	sys.exit(0)
      Hist.SetTitle("")
      if term == "Mu":
        Hist.GetXaxis().SetTitle("#mu")
      if term == "NPV":
        Hist.GetXaxis().SetTitle("N_{PV}")
      Hist.GetYaxis().SetTitle("#it{p}_{T}^{residual} [GeV]")
      if not Hist:
        print HistName + " not found, exiting"
	sys.exit(0)
      # Get Fit
      Hist.GetFunction(HistName+"_myfit").SetLineColor(kRed)
      Hist.GetFunction(HistName+"_myfit").SetMarkerColor(kRed)
      # Draw
      Hist.Draw()
      # Show eta bin
      if ieta==0:
         etaBin = "|#eta|<"+str(EtaBins[ieta+1])
      else:
         etaBin = str(EtaBins[ieta])+"<|#eta|<"+str(EtaBins[ieta+1])
      TextBlock_Eta = TLatex(0.2,0.88,etaBin)
      TextBlock_Eta.SetNDC()
      TextBlock_Eta.Draw("same")
      # Show NPV/mu bin
      Bin = "N_{PV} bin " + str(ibin+1)
      if term == "NPV":
        Bin = "#mu bin " + str(ibin+1)
      TextBlock_Bin = TLatex(0.2,0.83,Bin)
      TextBlock_Bin.SetNDC()
      TextBlock_Bin.Draw("same")
      can.Print(outPDF)
  can.Print(outPDF+"]")


#################################
# dpTresidual/dTerm plots
# Loop over terms
for term in Terms:
  # TCanvas
  can = TCanvas()
  # Name of output PDF
  outPDF = PATH+"dpTresidualOVERd"+term+".pdf"
  can.Print(outPDF+"[")
  # Open file
  FileName = PATH + term + "Term" + Version + JetCollection + ".root"
  File     = TFile.Open(FileName,"READ")
  if not File:
    print FileName + " not found, exiting"
    sys.exit(0)
  # Loop over eta bins
  for ieta in range(0,nEtaBins):
    # Get TH1D
    HistName  = "eta_"
    HistName += str(ieta)
    if term == "Mu":
      HistName += "_pt_0_zx0_mean_myfit_par1"
    if term == "NPV":
      HistName += "_pt_0_zy0_mean_myfit_par1"
    Hist = File.Get(HistName)
    Hist.SetTitle("")
    if term == "NPV":
      Hist.GetXaxis().SetTitle("#mu")
      Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial N_{PV} [GeV]")
    if term == "Mu":
      Hist.GetXaxis().SetTitle("N_{PV}")
      Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial#mu [GeV]")
    if not Hist:
      print HistName + " not found, exiting"
      sys.exit(0)
    # Get Fit
    Hist.GetFunction(HistName+"_myfit").SetLineColor(kRed)
    Hist.GetFunction(HistName+"_myfit").SetMarkerColor(kRed)
    Hist.Draw()
    # Show eta bin
    if ieta==0:
       etaBin = "|#eta|<"+str(EtaBins[ieta+1])
    else:
       etaBin = str(EtaBins[ieta])+"<|#eta|<"+str(EtaBins[ieta+1])
    TextBlock_Eta = TLatex(0.2,0.88,etaBin)
    TextBlock_Eta.SetNDC()
    TextBlock_Eta.Draw("same")
    can.Print(outPDF)
  can.Print(outPDF+"]")

print ">>> ALL DONE <<<"
