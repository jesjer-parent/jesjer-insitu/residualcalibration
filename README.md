###############################################################
# ResidualCalibration:
Package to compute a 3D correlated residual Pile-Up substraction.


###############################################################
## Procedure to derive the pile-up residual calibration

1) Jets in the TTree(s) must satisfy the following (already implemented in IsolatedJetTree package)
   1a) Calibrated up to the area-based pile-up calibration
   1b) Rrequired to match an isolated truth jet and to be isolated
2) 3 sets of 3D Histograms are produced from the MC ntuples in (|eta|,pT^{truth}) bins:
   -Delta_pT(pT^{areaCorrected}-pT^{truth}) vs NPV vs Mu
   -pT^{truth} vs NPV vs Mu
   - pT^{areaCorrected} vs NPV vs Mu 
2.1) For each of these histograms, the mean of Delta_pT (pT^{truth} or pT^{areaCorrected}) is extracted as function of NPV and mu. The resulting distributions are smoothed using the TH2::Smooth method.
3) FIRST TERM OF THE CORRECTION (3D CORRECTION) 
    3.a) Plot the dependence of Delta_pT vs pT^{areaCorrected} in bins of |eta|-NPV-mu
    3.b) Fit these distributions with a linear+log(pT) function. The correction is given by the fit function.
4) SECOND TERM
    4.a) Plot the dependence of Delta_pT vs pT^{true} in bins of |eta|
    4.b) Fit these distrubutions with a linear function. The correction is given by the fit function.

## Setup and compilation

Computing is done in two steps, you need a config file (explanation below):

script: Histos3D.cxx --Root macro to read MC ntuples and generate 3D histograms needed to derive the calibration (step 2). This stage is the more time consuming. Compile and run it with:
```bash
root -l Histos3D.cxx+'("InputFile","ConfigFile","AreaOption","WeightOption")
```
-InputFile can be a single file (.root) or a list of files (.txt).
-AreaOption is a flag to set the Area subtraction. Options are "ApplyArea" and "NoArea"
-WeightOption is a flag to set if the weight of the events are used or not. Options are "ApplyWeight" and "NoWeight".
-An example config file can be found in the macros/ directory.

Then, from the output you can derive the correction. The first time you need to compile the code with:
```bash
g++ -o DeriveResidualCorr CorrelatedResidual.cxx `root-config --cflags --glibs`
```
After compiling, you run it:
```bash
./DeriveResidualCorr /PATH/TO/INPUT/FILE /PATH/TO/CONFIGFILE
```

################################################################
# Plot closure plots 
1) Jets should satisfy the same conditions as above.
2) 3D Histograms of Delta_pT(pT^{3DCorrected}-pT^{truth}) vs NPV vs Mu are produced in bins of (|eta|,pT^{truth})
3) Plot the dependence of Delta_pT vs NPV (mu) in bins of |eta|-pT^{truth}-mu(NPV)
4) Fit these distributions with a linear function
5) Take partialpT/partialNPV (partialpT/partialMu) from the fits and plot it as a function of mu(NPV) for each (|eta|,pT^{truth}) bin
6) Fit these distributions with a scalar (average over mu(NPV)) and plot the result of the fit as a function of |eta|, in bins of pT^{truth}

# Setup and compilation
Computing is done in two steps, you need a config file (explanation below):
script: Histos3DSubs3DCorrection.cxx --Root macro to read MC ntuples and generate 3D histograms needed to generate closure plots (step 2). This stage is the more time consuming. Compile and run it with:
```bash
$root -l Histos3DSubs3DCorrection.cxx+'("InputFile","ConfigFile","AreaOption","WeightOption","CorrectionParameterssFile")
```
    -Options are the same as before but adding the parameters file to compute the correction.

Then, from the output you can plot the closure (steps 3 - 6). Script:FitResiduals/FitResidual.cxx
The first time, you need to compile:
```bash
$g++ -o FitResidual FitResidual.cxx `root-config --cflags --glibs`
```
Now you can run the program as an executable:
```bash
$./FitResidual output_histos_File ConfigFile Term
```
    -Term is a flag to set "NPVTerm" or "MuTerm"

###############################################################
# Configuration file

ResidualCalibration works with one configuation file which should be located in residualcalibration/macros.
An example config file, named ExampleConfig.config, can be found there.

The following information needs to be set:

Use "Version" to set calibration type (for example: MC16a_22102018)
Use "JetAlgos" to choose jet collection types (for example: AntiKt4EMTopoJets)
Use "TreeName" to choose the name of the TTree
Use "ResidualAbsEtaBins" to choose the |eta| binning (for example: 0 0.9 1.2 1.5 1.8 2.4 2.8 3.2 3.5 4.0 4.3 6.0)
Use "WeightBranch" to choose the branch for event weight.
Use "Calo*" variables to set the name of the branches with the jet reco information (pT, Eta, etc)
Use "True*" variables to set the name of the branches with the jet truth information (pT, Eta, etc)
Use "Fit*" variables to choose fit function and fit range used to fit pTresidual vs Mu (NPV)
Use "Average*" variables to choose fit function and fit range used to fit dpTresidual/dMu (dpTresidual/dNPV)
Use "HPar0*" variables to set pTReco-pTtruth and the binning on pTresidual (includes the subtraction (see below))
Use "PtDensity" to set the name of the branch for pT median density for Pile Up Area subtraction
Use "JetArea" to set the name of branch for jet Area for Pile Up Area subtraction
Use "NPV*" variables to set the binning on NPV
Use "mu*" variables to set the binning on Mu
Use "IPar0*" variables to set the binning on pT truth
Use "IPar0Fit*" variables to set the fit function and fit range used to fit pTresidual vs pT
Use "IPar1*" variables to choose the jet truth |eta| binning