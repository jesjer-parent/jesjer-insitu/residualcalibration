#include <string>
#include <cstring>
#include <sstream>

int GetBCID2(int bcid, float edges[]){
  int bcid2=-1;
  for (unsigned int i=0; i<18; i++){
    if (bcid>=edges[i] && bcid<edges[i+1])
      bcid2=bcid-edges[i];
  }
  return bcid2;
}

TList* DefinePtEtaHistos(Int_t nEtaBins, Int_t nPtBins, Int_t nXbins, Float_t Xmin, Float_t Xmax, Int_t nYbins, Float_t Ymin, Float_t Ymax){
  TList* l_PtEtaHistos = new TList();
  for(Int_t i = 0; i<nEtaBins; ++i){
    for(Int_t j = 0; j<nPtBins; ++j){
      TString name("BCID_Etabin_"+TString::Format("%d",i)+"Ptbin"+TString::Format("%d",j));
      TH2D* EtaPtHisto = new TH2D(name,"",nXbins,Xmin,Xmax,nYbins,Ymin,Ymax);
      l_PtEtaHistos->Add(EtaPtHisto);
    }
  }
  return l_PtEtaHistos;
}
