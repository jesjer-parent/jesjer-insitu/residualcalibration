#define __GET_TRIM_LOW_STATS 1
#define __GET_TRIM_CUT 100
#include <fstream>
#include "../macros/CalibTools.h"
#include <string>
#include <cstring>
#include <sstream>
#include <math.h>

TH1* GetMean2D(TH3F* h, TString name){

  Int_t nxbins = h->GetXaxis()->GetNbins();
  Int_t nybins = h->GetYaxis()->GetNbins();
  Int_t nzbins = h->GetZaxis()->GetNbins();
  TH1* haux = h->Project3D("YX");
  TH1* h_mean = (TH1D*) haux->Clone(name);
  h_mean->Reset();
  
  for(int i = 1; i <= nxbins; ++i){
    // std::cout<<"BINWIDTH: "<<h_mean->GetBinWidth(i)<<std::endl;
    h->GetXaxis()->SetRange(i,i);
    for (int j = 1; j <= nybins; ++j){
      h->GetYaxis()->SetRange(j,j);
      if(!__GET_TRIM_LOW_STATS || h->GetEffectiveEntries() > __GET_TRIM_CUT){	
//	h_mean->SetBinContent(i, j, h->GetMean(3));
  float mean = h->GetMean(3);
	h_mean->SetBinContent(i, j,mean);
	h_mean->SetBinError(i, j, h->GetMeanError(3));
      }
      else
	h_mean->SetBinContent(i, j, -999.9); //Setting a non physics value for low stats bins
    }
  }
  return h_mean; 
}


TList* GetMean2D(TList* list){
  Int_t nhistos = list->GetEntries();
  TList* listofmeans = new TList();
  for (int i = 0; i<nhistos; ++i){
    listofmeans->Add(GetMean2D((TH3F*) list->At(i),((TH3F*) list->At(i))->GetName())); //Computing mean plot for each histogram in the list
  }
  return listofmeans;
}

TList* GetpTtruedependence(TList* EtaListofMeanDistributions, std::vector<float> ptbins){ 
  int nhistograms = EtaListofMeanDistributions->GetSize(); 
  std::vector<TString> name;
  int nxbins = ((TH2F*) EtaListofMeanDistributions->At(0))->GetXaxis()->GetNbins();
  int nybins = ((TH2F*) EtaListofMeanDistributions->At(0))->GetYaxis()->GetNbins();
  int nptbins = ptbins.size()-1;
  int nEtabins = nhistograms/nptbins;
  if (nhistograms%nptbins != 0){
    std::cout<<"Incorrect number of pT bins"<<std::endl;
    return 0;
  }
  float* auxpTarray = &ptbins[0]; //auxiliar pointer to the array the vector of pT-true bins uses
  TList* listofpTdistributions = new TList();
  for(int etacount = 0; etacount<nEtabins; ++etacount){ //Loop over eta bins
  //  for(int etacount = 0; etacount<1; ++etacount){ //Loop over eta bins
    for(int xbin = 0; xbin<nxbins; ++xbin){ //Loop over x bins (mu)
      for(int ybin = 0; ybin<nybins; ++ybin){ // Loop over y bins (NPV)
	name.push_back("eta_"+TString::Format("%i",etacount)+"mu_"+TString::Format("%i",xbin)+"_NPV_"+TString::Format("%i",ybin));
	TH1F* muNPVhisto = new TH1F(name.back(),"pt-true dependence for mu-NPV bin",nptbins,auxpTarray);
	listofpTdistributions->Add(muNPVhisto);
	for(int i = 0; i<nptbins; ++i){ //loop over pt histograms of 1 eta bin
	  Double_t pTtruemean = ((TH2F*) EtaListofMeanDistributions->At(etacount*nptbins+i))->GetBinContent(xbin+1,ybin+1);
	  //	  std::cout<<"Histo at etacount*nptbins+i : "<<((TH2F*) EtaListofMeanDistributions->At(etacount*nptbins+i))->GetName()<<std::endl;
	  //	  std::cout<<" mu bin: "<<xbin+1<<"   NPV bin: "<<ybin+1<<"  pTtrue: "<<pTtruemean<<std::endl;
	  if(pTtruemean !=0 && pTtruemean != -999){
	    muNPVhisto->SetBinContent(i+1,pTtruemean); //bins go from 1 to nbins
	  }
	}
      }
    }
  }
  return listofpTdistributions;
}



void GetNPVmuCorrection(TList* EtaListofMeanDistributions, std::vector<float> ptbins){ 
  int nhistograms = EtaListofMeanDistributions->GetSize(); //number of histograms
  int nxbins = ((TH2F*) EtaListofMeanDistributions->At(0))->GetXaxis()->GetNbins(); //number of mu bins
  int nybins = ((TH2F*) EtaListofMeanDistributions->At(0))->GetYaxis()->GetNbins(); //number of NPV bins
  int nptbins = ptbins.size()-1; //number of pT-true bins 
  int nEtabins = nhistograms/nptbins; //number of eta-true bins
  std::ofstream outputfile; //outputfile to write the correction values
  outputfile.open("NPVmuCorrection.txt");
  if (nhistograms%nptbins != 0){
    std::cout<<"Incorrect number of pT bins"<<std::endl;
  }
  
  for(int etacount = 0; etacount<nEtabins; ++etacount){ //Loop over eta bins
    //    TH2F* muNPVcorrection = TH2F("pT area vs ");
    outputfile<< "etaBin"<<etacount<<":  ";
    for(int xbin = 0; xbin<nxbins; ++xbin){ //Loop over x bins (mu)
      for(int ybin = 0; ybin<nybins; ++ybin){ // Loop over y bins (NPV)
	//	Double_t NPVmuCorrection = ((TH2F*) EtaListofMeanDistributions->At(etacount*nptbins+4))->GetBinContent(xbin+1,ybin+1); //Taking the fifth pTtrue bin ( 20<pt<30 ) for each eta bin //Wach out
	Double_t NPVmuCorrection = ((TH2F*) EtaListofMeanDistributions->At(etacount*nptbins))->GetBinContent(xbin+1,ybin+1); //Taking the fifth pTtrue bin ( 20<pt<30 ) for each eta bin //Wach out
	if(NPVmuCorrection !=0 && NPVmuCorrection != -999){
	  outputfile<<NPVmuCorrection<<" ";
	}
	else if(!NPVmuCorrection || NPVmuCorrection==-999){
	  NPVmuCorrection = 0;
	  outputfile<<NPVmuCorrection<<" ";
	}
      }
    }
    outputfile<<std::endl;
  }
  outputfile.close();
}  

template < typename T > std::string to_string( const T& n ){   //function to convert numbers to strings
  std::ostringstream stm ;
  stm << n ;
  return stm.str() ;
}

template < typename T, typename U> std::vector<T> GetBinVector(T minvalue, T maxvalue , U nbins ){
  T space = (maxvalue-minvalue)/nbins; 
  std::vector<T> binsvector;
  for (int i = 0; i<=nbins; ++i){
    binsvector.push_back(minvalue+i*space);
  }
  return binsvector;
}


std::vector<std::vector <Double_t> >  GetCorrectionMatrix(TEnv* CorrectionConfig, int netabins){
  std::vector<std::vector<Double_t> > corrections;
  for(int i = 0; i<netabins; ++i){
    std::string etabin = "etaBin"+to_string(i);
    char *auxarray = new char[etabin.length() + 1];
    std::strcpy(auxarray, etabin.c_str());
    //    TString etabin("etaBin"+TString::Format("%i",i));
    //    corrections.push_back(VectorizeD(CorrectionConfig->GetValue(auxarray),"0 0 0 0 0 0 0"));
    corrections.push_back(VectorizeD(CorrectionConfig->GetValue(auxarray,"0 0 0 0 0 0 0")));
  }
  return corrections;
}



Double_t GetCorrectionValue(std::vector<float>& etaBins, std::vector<Float_t>& NPVbins, std::vector<Float_t>& mubins, Float_t eta, Float_t mu, Float_t NPV, std::vector<std::vector<double> >& correctionMatrix){
  int etabin, mubin, NPVbin;
  int nNPVbins = NPVbins.size()-1;
  //std::cout<<"Number of NPV bins = "<<nNPVbins<<std::endl;
  Double_t correction=0;
  for(unsigned int i=0; i<(etaBins.size()-1);++i){
    if (eta >= etaBins.at(i) && eta < etaBins.at(i+1)){
      etabin = i;
      for(unsigned int m =0; m<(mubins.size()-1); ++m){
	if(mu >= mubins.at(m) && mu < mubins.at(m+1)){
	  mubin = m;
	  for(unsigned int n = 0; n < (NPVbins.size()-1); ++n){
	    if (NPV >= NPVbins.at(n) && NPV < NPVbins.at(n+1)){
	      NPVbin = n;
	      correction = correctionMatrix[etabin][mubin*nNPVbins+NPVbin];
//	      std::cout<<"for eta = "<<eta<<" eta bin#: "<<etabin<<std::endl;
//	      std::cout<<"for mu = "<<mu<<" mu bin#: "<<m<<std::endl;
//	      std::cout<<"for NPV = "<<NPV<<" NPV bin#: "<<NPVbin<<std::endl;
//	      std::cout<<"correction at eta,NPV,mu = "<<correction<<" matrix entrie#: "<<etabin<<"  "<<mubin*nNPVbins+NPVbin<<std::endl;
	    }
	  }
	}
      }
    }
  }
  return correction;
}

TList* PlotTH2FitParams(TList* listofFits,TH2* templateHisto, int nEtabins, int order = 1){ //More inputs can be needed later

  TList* inters_list = new TList(); 
  inters_list->SetName("intersections");
  TList* slopes_list = new TList();  
  slopes_list->SetName("slopes");
  TList* secondOrder_list = new TList();  
  secondOrder_list->SetName("SecondOrderCoeff");
  TList* third_list = new TList();  
  third_list->SetName("Third");

  std::cout<<"template histo"<<std::endl;
  templateHisto->Print();

  TList* params_list = new TList();    
  int nHistos = listofFits->GetSize();
  std::cout<<"nHistos = "<<nHistos<<std::endl;
  int nMubins = templateHisto->GetXaxis()->GetNbins(); //number of mu bins
  std::cout<<"nMubins = "<<nMubins<<std::endl;
  int nNPVbins = templateHisto->GetYaxis()->GetNbins(); //number of NPV bins
  std::cout<<"nNPVbins = "<<nNPVbins<<std::endl;
  if (nHistos/(nEtabins*nMubins*nNPVbins) != 1){  
    std::cout<<"Incorrect number of eta bins"<<std::endl;
    return 0;
  }
  for(int etacount =0; etacount<nEtabins; ++etacount){
    TString nameSlope("slopes_eta"+TString::Format("%i",etacount));
    TString nameInters("intersections_eta"+TString::Format("%i",etacount));
    TH2D* slope = (TH2D*) templateHisto->Clone(nameSlope);
    slope->Reset();
    TH2D* inters = (TH2D*) templateHisto->Clone(nameInters) ;
    inters->Reset();
    TH2D* secondOrder = (TH2D*) templateHisto->Clone(nameInters) ;
    secondOrder->Reset();
    TH2D* third = (TH2D*) templateHisto->Clone(nameInters) ;
    third->Reset();
    for(int iMubins = 0; iMubins < nMubins; ++iMubins){
      for(int iNPVbins = 0; iNPVbins < nNPVbins; ++iNPVbins){
	int entrie = etacount*nMubins*nNPVbins+iMubins*nNPVbins+iNPVbins;
  if(((TF1*) listofFits->At(entrie))->GetParameter(0) == -999.9 && ((TF1*) listofFits->At(entrie))->GetParError(0)>0){
    std::cout<<"########################################################"<<std::endl;
    std::cout<<"Problematic fit"<<std::endl;
    std::cout<<"########################################################"<<std::endl;

  }
	inters->SetBinContent(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParameter(0));
	inters->SetBinError(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParError(0));

	slope->SetBinContent(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParameter(1));
	slope->SetBinError(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParError(1));
	if (order >1){
	  secondOrder->SetBinContent(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParameter(2));

	  secondOrder->SetBinError(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParError(2));
		//third->SetBinContent(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParameter(3));

	  //third->SetBinError(iMubins+1,iNPVbins+1,((TF1*) listofFits->At(entrie))->GetParError(3));
  }
      }
    }
    inters_list->Add(inters);    
    slopes_list->Add(slope);
    if (order >1){
      secondOrder_list->Add(secondOrder);
      third_list->Add(third);
    }
  }
  params_list->Add(inters_list);
  params_list->Add(slopes_list);  
  if (order >1){
    params_list->Add(secondOrder_list);
    params_list->Add(third_list);  
  }
  return params_list;
}


Double_t GetpTshift(Float_t eta, Float_t pT, TH2* pTdiffhisto){ //Shift from pT reco to mean of pT-true
  int binNumber = pTdiffhisto->FindBin(pT,eta);
  double pTshift = pTdiffhisto->GetBinContent(binNumber);
  
  return pTshift;
}//Now I don't need this (in principle)

//std::vector<Double_t> GetLinearFunctionParams(int etaBin, Float_t mu, Float_t NPV, Float_t pT, TList* ListofParams){
std::vector< std::vector<int> > m_closestNonEmptyAux;
//std::vector<TH2D*> m_3Dp0_vs_muNPV;

std::vector<Double_t> GetPolFunctionParams(int etaBin, Float_t mu, Int_t NPV, TList* ListofParams, Int_t nParams, Int_t muNPVbin = -1,std::vector< std::vector<int> > m_close = m_closestNonEmptyAux){
  //std::cout<<"auxhisto"<<std::endl;
  TH2D* Auxhisto = (TH2D*)((TList*)ListofParams->At(0))->At(0);
  int binNumber = Auxhisto->FindBin(mu,NPV);
//  std::cout<<"binnumber first = "<<binNumber<<std::endl;
  Double_t temp = ((TH2D*)((TList*)ListofParams->At(0))->At(etaBin))->GetBinContent(binNumber);
  if(muNPVbin>0 && temp<-999){
  //  std::cout<<"######################"<<std::endl;
  //  std::cout<<"setting closest bin"<<std::endl;
    //std::cout<<"etaBin = "<<etaBin<<std::endl;
    //std::cout<<"binNumber = "<<binNumber<<std::endl;
    binNumber = m_close[etaBin][binNumber];
    //std::cout<<"closest bin = "<<binNumber<<std::endl;
  }
  //  std::cout<<"Bin number: "<<binNumber<<std::endl;
  std::vector<Double_t> linearParams;
  Double_t inters = ((TH2D*)((TList*)ListofParams->At(0))->At(etaBin))->GetBinContent(binNumber);
  //std::cout<<"p0 = "<<inters<<std::endl;
  Double_t slope = ((TH2D*)((TList*)ListofParams->At(1))->At(etaBin))->GetBinContent(binNumber);
  //std::cout<<"p1 = "<<slope<<std::endl;
  Double_t second;
  Double_t third;
//  std::cout<<"computing second order param"<<std::endl;

  if(nParams >1){
    second = ((TH2D*)((TList*)ListofParams->At(2))->At(etaBin))->GetBinContent(binNumber);
//    third =  ((TH2D*)((TList*)ListofParams->At(3))->At(etaBin))->GetBinContent(binNumber);
  }
  if(muNPVbin>0){
  //  std::cout<<"etabin = "<<etaBin<<std::endl;
  //  std::"name = "<<((TH2D*)((TList*)ListofParams->At(2))->At(etaBin))->GetName()<<std::endl;
  //  std::cout<<"binNumber = "<<binNumber<<std::endl;
  //  std::cout<<"NPV = "<<NPV<<std::endl;
  //  std::cout<<"mu = "<<mu<<std::endl;
  //  std::cout<<"p0 = "<<inters<<std::endl;
  //  std::cout<<"p1 = "<<slope<<std::endl;
  //  std::cout<<"p2 = "<<second<<std::endl;
  }
  if(inters==-999.9 || slope==-999.9 || second==-999.9){
    if(muNPVbin>0) std::cout<<"bin at 0"<<std::endl;
    //std::cout<<"######################"<<std::endl;
    linearParams.push_back(0);
    linearParams.push_back(0);
    linearParams.push_back(0);
    return linearParams;
  }
  //std::cout<<"param = "<<second<<std::endl;
  linearParams.push_back(inters);
  linearParams.push_back(slope);
  if(nParams > 1){
    linearParams.push_back(second);
//    linearParams.push_back(third);
  }
  binNumber=0;
  return linearParams;
}

Float_t ComputeCorrValue(Float_t pT, std::vector<Double_t> linearParams, Int_t nParams){
  Double_t correction = 0;
  if (nParams == 1) correction = linearParams[0]+linearParams[1]*pT;
  //std::cout<<"correction"<<correction<<std::endl;
//  if (nParams == 1) correction = linearParams[0]+linearParams[1]/pT;
  //  if (nParams > 1)  correction = linearParams[0]+linearParams[1]*pT+linearParams[2]*pT*pT;
//  if (nParams > 1)  correction = linearParams[0]+linearParams[1]*log(pT)+linearParams[2]*log(pT)*log(pT);
 // if (nParams > 1)  correction = linearParams[0]+linearParams[1]*log(pT-linearParams[3])+linearParams[2]*log(pT-linearParams[3])*log(pT-linearParams[3]);
  //  if (nParams > 1)  correction = linearParams[0]+linearParams[1]*log(pT-linearParams[2]);
  if (nParams > 1)  correction = linearParams[0]+linearParams[1]*pT+linearParams[2]*log(pT);
  return correction;
}

//Double_t Get3DCorrection(Float_t eta, int etaBin, Float_t pTreco, Float_t mu, Int_t NPV, TH2* pTdiffhisto, TList* ListofParams ){
Float_t Get3DCorrection(Float_t eta, int etaBin, Float_t pTreco, Float_t mu, Int_t NPV, TList* ListofParams, Int_t nParams = 2, Int_t muNPVbin = -1, std::vector< std::vector<int> > m_close = m_closestNonEmptyAux){
  //  Double_t pTshift = GetpTshift(eta, pTreco, pTdiffhisto);
  //std::cout<<"3d correction"<<std::endl;
  //std::cout<<"size "<<m_close.size()<<std::endl;
  //std::cout<<"test "<<m_close[31][50]<<std::endl;
  std::vector<Double_t> linearParams = GetPolFunctionParams(etaBin, mu, NPV, ListofParams, nParams, muNPVbin,m_close);
  //  std::cout<<"intersection:"<<linearParams[0]<<std::endl;
  //  std::cout<<"slope:"<<linearParams[1]<<std::endl;
  //  Double_t correction = ComputeCorrValue(pTreco+pTshift, linearParams);
  Double_t correction = ComputeCorrValue(pTreco, linearParams, nParams);
  return correction;

}


TH2F* PlotPtMeans(TList* pTetaHistosList, std::vector<Float_t> EtaBins, std::vector<Float_t> PtBins){ 
  TH1::SetDefaultSumw2(kTRUE);
  int nHistos = pTetaHistosList->GetEntries();
  int nEtaBins = EtaBins.size()-1;
  int nPtBins = PtBins.size()-1;
  if (nPtBins != (nHistos/nEtaBins)) {
    std::cout<<"Incorrect EtaBins or PtBins in PlotPtMeans"<<std::endl;
    return 0;
  }
  TH2F* PtMeans = new TH2F("PtArea_trueMeans","PtArea_trueMeans",nEtaBins,&EtaBins[0],nPtBins,&PtBins[0]);

  for(int ieta = 0; ieta < nEtaBins; ++ieta){
    for(int iPt = 0; iPt < nPtBins; ++iPt ){
      Float_t pTmean = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMean(3);
      Float_t pTmeanError = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMeanError(3);
      //      std::cout<<"At ieta = "<<ieta<<",  iPt + "<<iPt<<"  histo:"<<((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetName()<<std::endl;  //For cross check and debugging
      //      std::cout<<"pT mean = "<<pTmean<<std::endl;
      PtMeans->SetBinContent(ieta+1,iPt+1,pTmean);
      PtMeans->SetBinError(ieta+1,iPt+1,pTmeanError);
    }
  }
  return PtMeans;
}


Double_t GetPtMean(TH2F* PtMeansHisto, Float_t pT, Float_t eta){
  Int_t binNumber = PtMeansHisto->FindBin(eta,pT);  
  Double_t PtMean = PtMeansHisto->GetBinContent(binNumber);
  std::cout<<"PtMean"<<std::endl;
  return PtMean;

}


TList* PlotEtaPtMeans(TList* pTetaHistosList, std::vector<Float_t> EtaBins, std::vector<Float_t> PtBins){ //mean of pTarea - pTtrue for each Eta-pT bin
  //  TH1::SetDefaultSumw2(kTRUE);
  int nHistos = pTetaHistosList->GetEntries();
  int nEtaBins = EtaBins.size()-1;
  int nPtBins = PtBins.size()-1;
  TF1* f = new TF1("linear","[0]+[1]*x",20,200);
  TList* l_PtMeans = new TList();
  if (nPtBins != (nHistos/nEtaBins)) {
    std::cout<<"Incorrect EtaBins or PtBins in PlotPtMeans"<<std::endl;
    return 0;
  }
  for(int ieta = 0; ieta < nEtaBins; ++ieta){
    TString name("etaBin"+TString::Format("%i",ieta));
    TH1F* PtMeanDist = new TH1F(name,"",nPtBins,&PtBins[0]);
    for(int iPt = 0; iPt < nPtBins; ++iPt ){
      Float_t pTmean = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMean(3);
      //      auto pTmean = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMean(3);
      Float_t pTmeanError = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMeanError(3);
      //auto pTmeanError = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMeanError(3);
      std::cout<<"At ieta = "<<ieta<<",  iPt = "<<iPt<<"  histo:"<<((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetName()<<std::endl;  //For cross check and debugging
      std::cout<<"pT mean = "<<pTmean<<std::endl;
      PtMeanDist->SetBinContent(iPt+1,pTmean);
      PtMeanDist->SetBinError(iPt+1,pTmeanError);
    }
    PtMeanDist->Fit(f);
    l_PtMeans->Add(PtMeanDist);
  }
  return l_PtMeans;
}


TList* PlotdPtMeansParameters(TList* EtaDpTMeans, std::vector<Float_t> EtaBins){ //I am going to edit this to produce 2 histos with the parameters of the linear fit 
  std::cout<<"writing delta pt params"<<std::endl;
  Int_t nHistos = EtaDpTMeans->GetEntries();
  Int_t nEtaBins = EtaBins.size()-1;
  std::cout<<"netabins = "<<nEtaBins<<std::endl;  
  std::cout<<"l429"<<std::endl;
  TList* DeltaPtParameters = new TList();
  //  Int_t nPtBins = PtBins.size()-1;
  if (nEtaBins != nHistos) {
    std::cout<<"Incorrect number of Eta bins "<<std::endl;
    return 0;
  }
  std::cout<<"l436"<<std::endl;
  TH1F* intersections = new TH1F("intersections","intersections",nEtaBins,&EtaBins[0]);
  TH1F* slopes = new TH1F("slopes","slopes",nEtaBins,&EtaBins[0]);
  std::cout<<"l439"<<std::endl;
  for(Int_t ieta = 0; ieta < nEtaBins; ++ieta){
    //Here i need to read the fit
    TF1* f_linear = ((TH1F*) EtaDpTMeans->At(ieta))->GetFunction("linear"); //This may cause problems because all the fits have the same name
    if(!f_linear) break;
    Float_t inters = f_linear->GetParameter(0);
    Float_t inters_err = f_linear->GetParError(0);
    Float_t slope =  f_linear->GetParameter(1);
    Float_t slope_err =  f_linear->GetParError(1);
    std::cout<<"At ieta = "<<ieta<<std::endl;  //For cross check and debugging
    //    std::cout<<"pT mean = "<<pTmean<<std::endl;
    intersections->SetBinContent(ieta+1,inters);
    intersections->SetBinError(ieta+1,inters_err);
    slopes->SetBinContent(ieta+1,slope);
    slopes->SetBinError(ieta+1,slope_err);
      
  }
  DeltaPtParameters->Add(intersections);
  DeltaPtParameters->Add(slopes);
  return DeltaPtParameters;
}


std::vector <Float_t> GetdPtMeanParameter(TList* DeltaPtParameters,Float_t eta){
  std::vector <Float_t> parameters;

  Int_t binNumber = ((TH1F*) DeltaPtParameters->At(0))->FindBin(eta);
  Float_t intersection = ((TH1F*) DeltaPtParameters->At(0))->GetBinContent(binNumber);
  Float_t slope = ((TH1F*) DeltaPtParameters->At(1))->GetBinContent(binNumber);
  
  parameters.push_back(intersection);
  parameters.push_back(slope);

  return parameters;

}

Float_t GetdPtMean(Float_t Pt, TList* DeltaPtParameters, Float_t eta){
  Int_t binNumber = ((TH1F*) DeltaPtParameters->At(0))->FindBin(eta);
  std::vector <Float_t> parameters = GetdPtMeanParameter(DeltaPtParameters, eta);
  Float_t DeltaPtMean = parameters[0] + parameters[1]*Pt;
  
  return DeltaPtMean;
}

  void fillEmptyBin(TH2* h,  float threshold){

    int nbinX = h->GetNbinsX();
    int nbinY = h->GetNbinsY();
    int nempty=1;
    int nTot= h->GetNcells();
    std::vector<float> tmpBufferC(nTot, threshold);
    std::vector<float> tmpBufferCErr(nTot, threshold);
    while(nempty>0){
      nempty = 0;
      for(int bi=0;bi<nTot;bi++) {
	int b = bi;
	if ( h->IsBinUnderflow(b) || h->IsBinOverflow(b) ) continue;
	if(h->GetBinContent(b)>threshold) continue;
	nempty++;
	int i0,j0,k0; h->GetBinXYZ(b,i0,j0,k0);
	int nn = 0;
	float m=0, m_err=0;
	for(int i=i0-1;i<i0+2;i++){
	  for(int j=j0-1;j<j0+2;j++){
	    if( (i<=0) or (i>nbinX)) continue;
	    if( (j<=0) or (j>nbinY)) continue;
	    float c = h->GetBinContent(i,j);
	    if(c>threshold){
	      m_err += h->GetBinError(i,j);
	      m += c; nn++;
	    }          
	  }
	}
	if(nn>2){
	  tmpBufferC[b] = m/nn;
	  tmpBufferCErr[b] = m_err/nn;
	  nempty--;
	}
      }
      if(nempty == (nbinY*nbinX) ) {
	std::cout<< " DJS::fillEmptyBin( ) : WARNING input histos fully empty "<< std::endl;
	return;
      }
      for(int bi=0;bi<nTot;bi++) {	
	if (tmpBufferC[bi]>threshold) {
	  //std::cout<< "  : fillin "<< bi << "  "<< tmpBufferC[bi] << std::endl;
	  h->SetBinContent(bi,tmpBufferC[bi]);
	  h->SetBinError(bi,tmpBufferCErr[bi]);	
	}
      }

    }

  }

void ExtrapolateCorr(TH2* h, float treshold){ //still working on this
  int nxbins = h->GetXaxis()->GetNbins();
  int nybins = h->GetXaxis()->GetNbins();

  for(int x=0; x<nxbins;++x){
    for(int y=0; y<nybins; ++y){
      double tempC = h->GetBinContent(x,y);
      if(tempC == treshold){
        std::cout<<"bad bin"<<std::endl;
        bool corr = false;
	      for(int i=1; i<nxbins+1; ++i){
	        if(i-x <1) continue;
	        else if(i-x>nxbins) continue;
	        else if(h->GetBinContent(i-x,y)==treshold) continue;
	        else{
            corr = true;
            h->SetBinContent(x,y,h->GetBinContent(i-x,y));
            break;
          }
        if(corr == false){  //When no correction on x axis
          for(int j=1; j<nybins+1;++j){
            if(j-y <1 ) continue;
            else if(j-y>nybins) continue;
            else if(h->GetBinContent(x,j-y)==treshold) continue;
            else{
              corr = true;
              h->SetBinContent(x,y,h->GetBinContent(x,j-y));
              break;
            }
            
          }

        }

	  //loop in y axis 
	      }
	
      }
      
    }
  }
}

//std::vector< std::vector<int> > m_closestNonEmpty;

std::vector<TH2D*> m_3Dp0_vs_muNPV;

 std::vector< std::vector<int> > setupClosestNonEmptyBins(TList* l_3Dp0_vs_muNPV){
      // Pre calculate the positions of the closest non empty bins,

      // we have a map (bin -> non-empty bin) for each eta slice :
  std::vector< std::vector<int> > m_closestNonEmpty;
  m_3Dp0_vs_muNPV.resize(l_3Dp0_vs_muNPV->GetSize());
  for(int iEtaBin=0; iEtaBin<l_3Dp0_vs_muNPV->GetEntries(); ++iEtaBin){
    m_3Dp0_vs_muNPV [iEtaBin] = (TH2D*) l_3Dp0_vs_muNPV->At(iEtaBin);
  }
  m_closestNonEmpty.resize( m_3Dp0_vs_muNPV.size() );
  for(size_t etabin=0;  etabin< m_closestNonEmpty.size() ;etabin++ ){

	TH2D *refHisto =  m_3Dp0_vs_muNPV[etabin] ;
	int nTot = refHisto->GetNcells();
	TAxis * xax = refHisto->GetXaxis();
	TAxis * yax = refHisto->GetYaxis();
	float xscale = 1./(xax->GetXmax()-xax->GetXmin()); xscale *= xscale;
	float yscale = 1./(yax->GetXmax()-yax->GetXmin()); yscale *= yscale;
	int nbinX = xax->GetNbins();
	int nbinY = yax->GetNbins();
	// initialize the map with -1 :
	m_closestNonEmpty[etabin].resize( nTot, -1 );

	// loop over each bin
	for(int xi=1;xi<nbinX+1;xi++) for(int yi=1;yi<nbinY+1;yi++) {
	    int bi = refHisto->GetBin(xi,yi);
	    if(refHisto->GetBinContent( bi ) >-999.) continue; // non empty bin, we don't need to find anything for it.

	    int clBin = -1;
	    float x0 = xax->GetBinCenter(xi);
	    float y0 = yax->GetBinCenter(yi);
	    float minDr2=1e10;
	    // loop over other bins to find the closest non-empty :
	    for(int xj=1;xj<nbinX+1;xj++) for(int yj=1;yj<nbinY+1;yj++) {
		int bj = refHisto->GetBin(xj,yj);
		if(refHisto->GetBinContent( bj ) <=-999.) continue; // this is an empty bin
		float x = xax->GetBinCenter(xj);
		float y = yax->GetBinCenter(yj);
		float dr2 = (x0-x)*(x0-x)*xscale+(y0-y)*(y0-y)*yscale;
		if(dr2<minDr2){ minDr2 = dr2; clBin = bj;}
	      }
	    m_closestNonEmpty[etabin][bi] = clBin;
      if(refHisto->GetBinContent(clBin)==0){
        std::cout<<"etabin = "<<etabin<<std::endl;
        std::cout<<"clBin = "<<clBin<<std::endl;
        std::cout<<"value at closest bin = "<<refHisto->GetBinContent(clBin)<<std::endl;
      }
      //std::cout<<"clBin = "<<clBin<<std::endl;
	  }

      }
      return m_closestNonEmpty;
    }

Float_t ComputeMeanTH2(TH2F* hist){

  int nxbins = hist->GetXaxis()->GetNbins();
  int nybins = hist->GetYaxis()->GetNbins();
  float nbins = 0.0;
  Float_t mean = 0;
  for(int xbin=0; xbin<nxbins; ++xbin){
    for(int ybin=0; ybin<nybins; ++ybin){
      if(hist->GetBinContent(xbin+1,ybin+1)>-999){
        mean += hist->GetBinContent(xbin+1,ybin+1);
        ++nbins;
      }
    }
  }
  mean = mean/(nbins);
  std::cout<<"mean TH2 = "<<mean<<std::endl;
  return mean;
}

Float_t ComputeMeanErrorTH2(TH2F* hist){
  int nxbins = hist->GetXaxis()->GetNbins();
  int nybins = hist->GetYaxis()->GetNbins();
  float nbins = 0.0;
  float error = 0.0;
  float temperr;
  for(int xbin=0; xbin<nxbins; ++xbin){
    for(int ybin=0; ybin<nybins; ++ybin){
      temperr = hist->GetBinError(xbin+1,ybin+1);
      if(temperr!=0){
        error += temperr*temperr;
        ++nbins;
      }
    }
  }
  error = sqrt(error)/nbins;
  std::cout<<"mean errror TH2 = "<<error<<std::endl;
  return error;
}

TList* PlotEtaPtMeans2D(TList* pTetaHistosList, std::vector<Float_t> EtaBins, std::vector<Float_t> PtBins){ //mean of pTarea - pTtrue for each Eta-pT bin
  //  TH1::SetDefaultSumw2(kTRUE);
  int nHistos = pTetaHistosList->GetEntries();
  int nEtaBins = EtaBins.size()-1;
  int nPtBins = PtBins.size()-1;
  TF1* f = new TF1("linear","[0]+[1]*x",20,200);
  TList* l_PtMeans = new TList();
  if (nPtBins != (nHistos/nEtaBins)) {
    std::cout<<"Incorrect EtaBins or PtBins in PlotPtMeans2D"<<std::endl;
    return 0;
  }
  for(int ieta = 0; ieta < nEtaBins; ++ieta){
    TString name("etaBin"+TString::Format("%i",ieta));
    TH1F* PtMeanDist = new TH1F(name,"",nPtBins,&PtBins[0]);
    for(int iPt = 0; iPt < nPtBins; ++iPt ){
      //Float_t pTmean = ((TH2F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMean(3);
      Float_t pTmean = ComputeMeanTH2((TH2F*) pTetaHistosList->At(ieta*nPtBins+iPt));

      //      auto pTmean = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMean(3);
//      Float_t pTmeanError = ((TH2F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMeanError(3);
//      Float_t pTmeanError = pTmean*0.1; //just to test 10% error
      Float_t pTmeanError = ComputeMeanErrorTH2((TH2F*) pTetaHistosList->At(ieta*nPtBins+iPt)); //just to test 10% error
      //auto pTmeanError = ((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetMeanError(3);
      std::cout<<"At ieta = "<<ieta<<",  iPt = "<<iPt<<"  histo:"<<((TH3F*) pTetaHistosList->At(ieta*nPtBins+iPt))->GetName()<<std::endl;  //For cross check and debugging
      std::cout<<"pT mean = "<<pTmean<<std::endl;
      PtMeanDist->SetBinContent(iPt+1,pTmean);
      PtMeanDist->SetBinError(iPt+1,pTmeanError);
    }
    PtMeanDist->Fit(f);
    l_PtMeans->Add(PtMeanDist);
  }
  return l_PtMeans;
}

