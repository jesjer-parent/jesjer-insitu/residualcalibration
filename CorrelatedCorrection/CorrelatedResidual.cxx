#define NEWRESIDUAL_H
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>

#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TList.h"
#include "TEventList.h"
#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TSystem.h"
#include "TColor.h"
#include "TChain.h"
#include "TLegend.h"
#include "TF2.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TH3F.h"
#include "TApplication.h"
//#include "../macros/CalibTools.h"
#include "TStyle.h"
#include "TEnv.h"
#include "CorrelatedResidual.h"
#include "../macros/ResidualTools.h"

int main (int argc, char * argv[]){
//Reading file TH3 from inputfile
  TString InputFile  (argv[1]);
  std::cout<<"InputFile: "<<InputFile<<std::endl;
  TFile* file = TFile::Open(InputFile);
  if(!file){
    std::cout << InputFile << " not found, exiting" << std::endl;
    return 0;
  }
  //Reading  Lists of histograms from file
  TList *list = (TList*)file->Get("DeltaPtHistList"); // list is arrayh3d in Histo3D == TH3F with (pt-areaxrho-pt_true = delta_pt, mu, NPV)
  if(!list){
    std::cout <<"List not found, exiting" << std::endl;
    return 0;
  }
  std::cout<<"DeltaPtHistList read"<<std::endl;

TList* pTtrueHistList = (TList*) file->Get("pTtrueHistList");
  if(!pTtrueHistList){
    std::cout <<"pTtrueHistList  not found, exiting" << std::endl;
    return 0;
  }
  std::cout<<"list read"<<std::endl;

TList* pTuncalibHistList = (TList*) file->Get("pTuncalibHistList"); //pT-area 
  if(!pTuncalibHistList){
    std::cout <<"pTtrueHistList  not found, exiting" << std::endl;
    return 0;
  }
  std::cout<<"list read"<<std::endl;

  TH1::SetDefaultSumw2(kTRUE);
  //TList* listofmeans = GetMean2D(list); //Creating list of mean distributions // seems not needed now 20-07-2020

  //Reading config file
  TString configname (argv[2]);
  TEnv* config = new TEnv(configname);
  int nptbins = config->GetValue("TruePtNBin",1);
  TString ptvariable("TruePtBin"); //Later change it to read from config
  std::vector<float> ptbins=ReadBinning(config,ptvariable,nptbins); //Reading pt binning from config
//  TList* muNPV_ptTrue_ptsubs = GetpTtruedependence(listofmeans, ptbins); //seems not needed now 20-07-2020

  TString etavariable("TrueEtaBin"); //Later change it to read from config
  int nEtabins = config->GetValue("TrueEtaNBin",38);
  std::vector<float> etabins = ReadBinning(config,etavariable,nEtabins);
 
  list->Print();

  //GetNPVmuCorrection(listofmeans,ptbins); //seems not needed now 20-07-2020

std::string thoption = argv[3];
  std::cout<<"thoption = "<<thoption<<std::endl;
  TList* l_PtMeans = NULL;
  TList* listofTGraphs = NULL;
  if(thoption == "th3") l_PtMeans = PlotEtaPtMeans(list,etabins,ptbins); // build TH1F representing (mean delta_pt) vs pt_bin , 1 TH1F per eta slice. TH1F are fitted with a linear func
  else if(thoption == "th2") l_PtMeans = PlotEtaPtMeans2D(list,etabins,ptbins); // build TH1F representing (mean delta_pt) vs pt_bin , 1 TH1F per eta slice. TH1F are fitted with a linear func
  l_PtMeans->Print();
  //  TH2F* PtArea_trueMeans = PlotPtMeans(list,etabins,ptbins);
  TList* l_dPtMeanParams = PlotdPtMeansParameters(l_PtMeans, etabins); // buils 2 TH1F vs eta : slope[(delta_pt mean) vs pt_bin] vs eta and intersec[(delta_pt mean) vs pt_bin] vs eta

//  Generating TGraphs for pt-true and pt-uncalibrated,  the values area stored in a TH2
  //  TList* listofTGraphs = pTcorrelation_eta(file,nptbins,ptbins,etabins);
  if(thoption == "th3") listofTGraphs = pTcorrelation(file,nptbins); // --> 2 lists of TGraphError. 1st/2nd list = (mean delta_pt vs mean_pt_true/calib) 1 per (eta,mu,NPV) bin
  else if(thoption == "th2") listofTGraphs = PlotTGraphsDeltaPtvsPtAfterArea(file,nptbins); // --> 2 lists of TGraphError. 1st/2nd list = (mean delta_pt vs mean_pt_true/calib) 1 per (eta,mu,NPV) bin

TList* pTareaDist = (TList*) listofTGraphs->At(1); //<--------------
pTareaDist->Print();
  //Here I need to Fit the TGraphs for pTarea
  // Read Fit Function and Range
  TString pTareaFitString = config->GetValue("FitPtArea","[0]+[1]*x");
  std::cout<<"Fit pT(area): "<<pTareaFitString<<std::endl;
  float FitHighpTtrue = config->GetValue("TruePtFitHigh",200.0);  // change names in config
  float FitLowpTtrue = config->GetValue("TruePtFitLow",10.0);  // change names in config
  std::cout<<"FitLow: "<<FitLowpTtrue<<std::endl;
  std::cout<<"FitHigh: "<<FitHighpTtrue<<std::endl;
  std::cout<<"initialize TF1 'pT true fit'"<<std::endl;
  //  TF1* pTareafit = new TF1("pTareafit",pTareaFitString,FitLowpTtrue,FitHighpTtrue);
  TF1* pTareafit = new TF1("pTareafit",pTareaFitString,FitLowpTtrue,FitHighpTtrue);
  pTareafit->SetParameter(0,0.1); pTareafit->SetParameter(1,0.0); //Seed values 
  pTareafit->SetParameter(2,0.0);

  std::cout<<"Building fits delta_pT vs pT-area"<<std::endl;
  //  TList* pTareafit_list = PTools::Fit(muNPV_ptTrue_ptsubs,pTtruefit,1,"R"); //Fittings pt-true dependence
  TList* pTareafit_list = PTools::FitTGraphs(pTareaDist,pTareafit,1,"R"); //Fittings pt-true dependence

  TList* parameters_list = NULL;
  //  TList* parameters_list = PlotTH2FitParams(pTareafit_list,(TH2*)(((TH3F*)list->At(0))->Project3D("YX")),nEtabins,2);
  if(thoption == "th3") parameters_list = PlotTH2FitParams(pTareafit_list,(TH2*)(((TH3F*)list->At(0))->Project3D("YX")),nEtabins,2);
  if(thoption == "th2") parameters_list = PlotTH2FitParams(pTareafit_list,(TH2*)list->At(0),nEtabins,2);

  TString ResidualVersion = config->GetValue("Version","VERSION");
  TString JetAlgo = config->GetValue("JetAlgos","AntiktEM_");
  TString name("3DcorrParams_"+ResidualVersion+JetAlgo+".root");

  TFile* fmeans = new TFile(name,"RECREATE");
//  listofmeans->Print();  //seems not needed now 20-07-2020
//  listofmeans->Write();  //seems not needed now 20-07-2020
//  muNPV_ptTrue_ptsubs->Write();  //seems not needed now 20-07-2020
  listofTGraphs->Write();
  parameters_list->Write("parameters",TObject::kSingleKey);
  l_PtMeans->Write("EtaPtMeans",TObject::kSingleKey);
  l_dPtMeanParams->Write("dPtParams",TObject::kSingleKey);
  fmeans->Close();

  // Write a single ROOT file containing only the parameters needed to apply calibration
  TFile calibParametersF("residualCalibParameters.root", "recreate");
  calibParametersF.cd();
  parameters_list->Write("param3D",TObject::kSingleKey);
  l_dPtMeanParams->Write("paramDeltaPt",TObject::kSingleKey);
  // also write out the etabins, since this is a required information.
  std::cout<<"etabins size = "<<etabins.size()<<std::endl;
  for(int i=0;i<etabins.size();++i){std::cout<<"etabin "<<i<<" = "<<etabins[i]<<std::endl;}
  calibParametersF.WriteObject( &etabins, "etaBins");
  calibParametersF.Close();
  return 0;
}
