#!/usr/bin/env python
from optparse import OptionParser
from ROOT import *
import math
import sys
#import testPUCorrection as tpu

#import numpy as np
gInterpreter.LoadText( open("../CorrelatedCorrection/CorrelatedResidual.h").read() )
#gROOT.ProcessLine(".L loader.C+")


def ParseOptions():
    p = OptionParser()
    p.add_option('-i', '--input', type='string', default="input.txt", help='input file with full paths of trees')
    p.add_option('-o', '--output', type='string', default="out.root", help='output file')

    (opts,args) = p.parse_args()
    return opts

def GetMeanZaxis(histo):
    projection = histo.Project3D("yx")
    z_mean = 0
    z_meanerr = 0
    name = histo.GetName()
    etabin = int(name.split('_')[2])
    ptbin = int(name.split('_')[4])

    for xbin in range(1,histo.GetXaxis().GetNbins()+1):
        for ybin in range(1,histo.GetYaxis().GetNbins()+1):
            histo.GetXaxis().SetRange(xbin,xbin)
            histo.GetYaxis().SetRange(ybin,ybin)
            if histo.GetEffectiveEntries() > 100:
                z_mean = histo.GetMean(3)
                z_meanerr = histo.GetMeanError(3)
            else:
                z_meanerr = 0
                z_mean = -999.9

            projection.SetBinContent(xbin,ybin,z_mean)
            projection.SetBinError(xbin,ybin,z_meanerr)
            z_mean = 0
    
    return projection

def Hist3Dto2D(l_hist):
    l2d = TList()
    for i in range(l_hist.GetEntries()):
        tmpHist = GetMeanZaxis(l_hist.At(i))
        l2d.Add(tmpHist)
    return l2d

def SmoothHists(l2d):
    for i in range(0,l2d.GetEntries()):
        l2d.At(i).Smooth()
def ExtrapBinContent(histo,clbins,etabin):
    for xbin in range(1,histo.GetXaxis().GetNbins()+1):
        for ybin in range(1,histo.GetYaxis().GetNbins()+1):
            temp = histo.GetBinContent(xbin,ybin)
            tempErr = histo.GetBinError(xbin,ybin)
            if temp < -999:
                xax = histo.GetXaxis()
                yax = histo.GetYaxis()
                x = xax.GetBinCenter(xbin)
                y = yax.GetBinCenter(ybin)
                binnumber = histo.FindBin(x,y)
#                print 'binnumber = ' + str(binnumber)
                temp = histo.GetBinContent(clbins[etabin][binnumber])
                tempErr = histo.GetBinError(clbins[etabin][binnumber])
#                print 'temp = ' + str(temp)
                #histo.SetBinContent(xbin,ybin,temp)
                if temp == -999.9: 
                    name = histo.GetName()
                    print name
                    print 'etabin = ' + str(etabin)
                    print 'xbin = ' + str(xbin)
                    print 'ybin = ' + str(ybin)
#                    print 'bin number = ' + str(binnumber)
                    print 'closest bin = ' + str(clbins[etabin][binnumber])
                    a= Long()
                    b= Long()
                    c= Long()
                    d=Long(clbins[etabin][binnumber])
                    histo.GetBinXYZ(d,a,b,c)
                    print 'xbin close = ' + str(a)
                    print 'ybin close = ' + str(b)
                    print 'content = ' + str(histo.GetBinContent(a,b))
            histo.SetBinContent(xbin,ybin,temp)
            histo.SetBinError(xbin,ybin,tempErr)


def ExtrapHistsInList(l2d,clbins):
    for i in range(l2d.GetEntries()):
        ExtrapBinContent(l2d.At(i),clbins,i)

def main(inpf,out):
    fin = TFile.Open(inpf)
    l = fin.Get("DeltaPtHistList")
    l2d = Hist3Dto2D(l)
    clbins = setupClosestNonEmptyBins(l2d) #map of closest bins to extrapolate distributions
    ExtrapHistsInList(l2d,clbins)
    SmoothHists(l2d)

    ltrue = fin.Get("pTtrueHistList")
    ltrue2d = Hist3Dto2D(ltrue)
    ExtrapHistsInList(ltrue2d,clbins)
    SmoothHists(ltrue2d)

    luncalib = fin.Get("pTuncalibHistList")
    luncalib2d = Hist3Dto2D(luncalib)
    ExtrapHistsInList(luncalib2d,clbins)
    SmoothHists(luncalib2d)

    fout = TFile(out,"recreate")
    l2d.Write("DeltaPtHistList",TObject.kSingleKey)
    ltrue2d.Write("pTtrueHistList",TObject.kSingleKey)
    luncalib2d.Write("pTuncalibHistList",TObject.kSingleKey)

    fout.Close()

if __name__ == '__main__':
    opts = ParseOptions()
    main(opts.input,opts.output)