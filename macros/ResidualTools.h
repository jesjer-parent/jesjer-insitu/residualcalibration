#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "vector"
#include "vector"
#include "TEnv.h"
#include "TGraph.h"
#include "TGraphErrors.h"


//Function to read the binning from the config file
std::vector<float> ReadBinning(TEnv* configfile, TString variable, int nbins){
  std:: vector<float> binsvector;
  for(int ibin=0; ibin<=nbins;++ibin){ //Looping and getting bin values from config
    binsvector.push_back(configfile->GetValue(variable+TString::Format("%i",ibin),(float)ibin));
  }
  return binsvector;
}

//Define the histograms to be used in an array and add them to a TList
void DefineHistos(int netabins, int nptbins, TH3F* arrayh3d[], TList* l,int nxbins,float xlow,float xhigh,int nybins,float ylow,float yhigh,int nzbins,float zlow,float zhigh, std::string text){ 
  std::vector<TString> name;
  TString prefix(text);
  for (int etacount=0; etacount<netabins;++etacount){
    for(int ptcount=0;ptcount<nptbins;ptcount++){
      name.push_back(prefix+"eta_"+TString::Format("%i",etacount)+"_pt_"+TString::Format("%i",ptcount));
      arrayh3d[etacount*nptbins+ptcount]=new TH3F(name[etacount*nptbins+ptcount],"pTresidual vs NPV vs Mu",nxbins,xlow,xhigh,nybins,ylow,yhigh,nzbins,zlow,zhigh);
      arrayh3d[etacount*nptbins+ptcount]->Sumw2();

      l->Add(arrayh3d[etacount*nptbins+ptcount]);
    }
  }
  std::cout<<"Histos defined"<<std::endl;
}

//Reading the name of the branches to be used from the config file
std::vector <TString> ReadBranches(TEnv* configfile){
  std::vector <TString> branches;
  branches.push_back(configfile->GetValue("TruePt",""));
  branches.push_back(configfile->GetValue("CaloPt",""));
  branches.push_back(configfile->GetValue("NPV","")); //NPV
  branches.push_back(configfile->GetValue("mu","")); //mu
  branches.push_back(configfile->GetValue("PtDensity","")); 
  branches.push_back(configfile->GetValue("JetArea","")); 
  branches.push_back(configfile->GetValue("TrueEta","")); 
  branches.push_back(configfile->GetValue("WeightBranch","weight"));
  branches.push_back(configfile->GetValue("CaloEta","jet_ConstitEta"));
  branches.push_back(configfile->GetValue("BCID","bcid"));
  std::cout<<"Variables read"<<std::endl;
  return branches;
}

//This do not work, addresses are lost when going out of the scope
void Declare_variables(std::vector<float>* truejet_pt, std::vector <float>* recojet_pt, Int_t NPV, Float_t mu, Double_t rho, std::vector <float>*area_pt,  std::vector<float>* reco_eta, TTree* fileschain, std::vector <TString> branches){

  truejet_pt=0;

  fileschain->SetBranchAddress(branches[0], &truejet_pt);
  fileschain->SetBranchAddress(branches[1], &recojet_pt);
  fileschain->SetBranchAddress(branches[2], &NPV);
  fileschain->SetBranchAddress(branches[3], &mu);
  fileschain->SetBranchAddress(branches[4], &rho);
  fileschain->SetBranchAddress(branches[5], &area_pt);
  fileschain->SetBranchAddress(branches[6], &reco_eta);

  std::cout<<"branches set"<<std::endl;
}

//Interpolation for the values of mu and NPV terms 
std::vector<double> computeoffsets(std::vector<double> etaBins, std::vector<double> term){
  std::vector <double> offset;
  offset.push_back(term.at(0));  
  for(unsigned int i=1; i<etaBins.size();++i){
    double espace = etaBins.at(i)-etaBins.at(i-1);
    double ioffs = offset.at(i-1)+term.at(i)*espace;
    offset.push_back(ioffs);
  }
  return offset;
}

double interpolation (std::vector<double> etaBins, std::vector<double> term, std::vector<double> offset, Float_t eta){
  double correction=0;
  //  std::cout<<"eta : "<<eta<<std::endl;
  for(unsigned int i=0; i<(etaBins.size()-1);++i){
    if (eta >= etaBins.at(i) && eta < etaBins.at(i+1)){
      correction = offset.at(i) + (eta-etaBins.at(i))*term.at(i+1); // computing value of the linear function at bin i for a given value of eta
      //  std::cout<<"inside interpolation if"<<std::endl;
      break;
    }
  }
  return correction;      
}

TList* pTcorrelation(TFile* fileTH3s, int npTbins){  
  //TList that will contain TList results
  TList* TGraphsLists = new TList();
  //TList for pTtrue
  TList* pTtrueGraphs = new TList();
  //TList for pTuncalib
  TList* pTuncalibGraphs = new TList();
  //Reading TList's of TH3's from input file
  TList * pTdiffHistList = (TList*) fileTH3s->Get("DeltaPtHistList"); //I will need to change the name
  if (!pTdiffHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  TList * pTtrueHistList = (TList*) fileTH3s->Get("pTtrueHistList");
  if (!pTtrueHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  TList * pTuncalibHistList = (TList*) fileTH3s->Get("pTuncalibHistList");
  if (!pTuncalibHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  //Number of bins
  int nhistograms = pTtrueHistList->GetSize();
  int nxbins = ((TH3F*) pTtrueHistList->At(0))->GetXaxis()->GetNbins();
  int nybins = ((TH3F*) pTtrueHistList->At(0))->GetYaxis()->GetNbins();
  int nEtabins = nhistograms/npTbins;
  if (nhistograms%npTbins != 0){
    std::cout<<"Incorrect number of pT bins"<<std::endl;
    return TGraphsLists;
  }

  for(int etacount = 0; etacount<nEtabins; ++etacount){ //Looping on eta
    for(int xcount = 0; xcount< nxbins; ++xcount){ //Looping on mu
      for(int ycount = 0; ycount < nybins; ++ycount){ //Looping on NPV
	std::vector<float> mean_pTarea_true;
	std::vector<float> mean_pTtrue;
	std::vector<float> mean_pTuncalib;
	std::vector<float> errX;
	std::vector<float> errY;
	TString name("eta_"+TString::Format("%i",etacount)+"_mu_"+TString::Format("%i",xcount)+"_NPV_"+TString::Format("%i",ycount));
	for(int ptcount = 0; ptcount < npTbins; ++ptcount){ //Loop on pT
	  //Reading means of z axis 

	    ((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xcount+1,xcount+1);
	    ((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ycount+1,ycount+1);
	  if (((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetEffectiveEntries()>100){
	    mean_pTarea_true.push_back(((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetMean(3));
	    errY.push_back(((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetMeanError(3));
	    ((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xcount+1,xcount+1);
	    ((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ycount+1,ycount+1);
	    mean_pTtrue.push_back(((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetMean(3));
	    ((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xcount+1,xcount+1);
	    ((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ycount+1,ycount+1);
	    mean_pTuncalib.push_back(((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetMean(3));
	    errX.push_back(((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetMeanError(3));
	  }
	}

	int npoints = mean_pTtrue.size();
	TGraph* pTtrueGraph = new TGraph(npoints, &mean_pTtrue[0], &mean_pTarea_true[0]);
	pTtrueGraph->SetName("pTtrue"+name);
	TGraphErrors* pTuncalibGraph = new TGraphErrors(npoints, &mean_pTuncalib[0], &mean_pTarea_true[0],&errX[0],&errY[0]); 
	pTuncalibGraph->SetName("pTuncalib"+name);
	pTtrueGraphs->Add(pTtrueGraph);
	pTuncalibGraphs->Add(pTuncalibGraph);
	mean_pTarea_true.clear();
	mean_pTtrue.clear();
	mean_pTuncalib.clear();
      }
    }
  }
  TGraphsLists->Add(pTtrueGraphs);
  TGraphsLists->Add(pTuncalibGraphs);
  return TGraphsLists;
}


TList* pTcorrelation_eta(TFile* fileTH3s, int npTbins,std::vector<float> pTbins, std::vector<float> Etabins){  //This is for only eta bins OUTDATED
  //TList that will contain TList results
  TList* TGraphsLists = new TList();
  //TList for pTtrue
  TList* pTtrueGraphs = new TList();
  //TList for pTuncalib
  TList* pTuncalibGraphs = new TList();
  //Reading TList's of TH3's from input file
  TList * pTdiffHistList = (TList*) fileTH3s->Get("TList"); //I will need to change the name
  if (!pTdiffHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  TList * pTtrueHistList = (TList*) fileTH3s->Get("pTtrueHistList");
  if (!pTtrueHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  TList * pTuncalibHistList = (TList*) fileTH3s->Get("pTuncalibHistList");
  if (!pTuncalibHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  //Number of bins
  int nhistograms = pTtrueHistList->GetSize();
  int nEtabins = nhistograms/npTbins;
  if (nhistograms%npTbins != 0){
    std::cout<<"Incorrect number of pT bins"<<std::endl;
    return TGraphsLists;
  }
  //Vectors to save the pT values
  std::vector<float> mean_pTarea_true;
  std::vector<float> mean_pTtrue;
  std::vector<float> mean_pTuncalib;
  //TH2 to save the differences in pT
  TH2D* pTdiff = new TH2D("pTtrue_pTuncalib","pTtrue - pTuncalib",npTbins,&pTbins[0],nEtabins,&Etabins[0]);
  for(int etacount = 0; etacount<nEtabins; ++etacount){ //Looping on eta
    TString name("eta_"+TString::Format("%i",etacount)+"_pTdependence");
    for(int ptcount = 0; ptcount < npTbins; ++ptcount){ //Loop on pT
      //Here I will later try to use the condition on stats
      //      Double_t pTtrue_pTuncalib = ((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetMean(3)+((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetMean(3);
      //      Double_t pTtrue_pTuncalib = -1.*((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetMean(3);
      Double_t pTtrue_pTuncalib = ((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetMean(3) - ((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetMean(3);
      //      std::cout<<"pTdiff Histo at etacount*npTbins+ptcount :"<< ((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
      //      std::cout<<"pTtrue Histo at etacount*npTbins+ptcount :"<< ((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
      //      std::cout<<"pTarea Histo at etacount*npTbins+ptcount :"<< ((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
      pTdiff->SetBinContent(ptcount+1,etacount+1,pTtrue_pTuncalib);
      mean_pTarea_true.push_back(((TH3F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetMean(3));
      mean_pTtrue.push_back(((TH3F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetMean(3));
      mean_pTuncalib.push_back(((TH3F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetMean(3));
    }
    int npoints = mean_pTtrue.size();
    TGraph* pTtrueGraph = new TGraph(npoints, &mean_pTtrue[0], &mean_pTarea_true[0]);
    pTtrueGraph->SetName("pTtrue"+name);
    TGraph* pTuncalibGraph = new TGraph(npoints, &mean_pTuncalib[0], &mean_pTarea_true[0]); 
    pTuncalibGraph->SetName("pTuncalib"+name);
    pTtrueGraphs->Add(pTtrueGraph);
    pTuncalibGraphs->Add(pTuncalibGraph);    
    mean_pTarea_true.clear();
    mean_pTtrue.clear();
    mean_pTuncalib.clear();
  }
  TGraphsLists->Add(pTtrueGraphs);
  TGraphsLists->Add(pTuncalibGraphs);
  TGraphsLists->Add(pTdiff);
  return TGraphsLists;
}


static TF1* Fit(TGraph* pTDistGraph, TF1* f,Option_t *option = "R"){
  TF1* fit = (TF1*)f->Clone(TString(pTDistGraph->GetName())+"_myfit");
  Int_t status = 1;
  //    status = h->Fit(fit,"R");                                                                  
  //    status = h->Fit(fit,"R WL"); ///<-------------------------------------                     
  status = pTDistGraph->Fit(fit,option); ///<-------------------------------------                           
  //don't restrict range                                                                           
  //status = h->Fit(fit);                                                                          
  if(status){
    fit->SetParameter(0, -999.9);
    std::cout<<"BADFIT!"<<std::endl;
  }
  return fit;
}



TList* PlotEtaDependence(TList* averageFits,std::vector<float> etabins){ //averageFits contains nEtaBins TList's of npTbins TH1's

  int nEtaBins = etabins.size()-1;
  float EtaBinsArray[nEtaBins];
  for(int ibin = 0; ibin<nEtaBins; ++ibin) EtaBinsArray[ibin] = etabins.at(ibin);

  std::cout<<"nEtaBins"<<nEtaBins<<std::endl;
  //  for(int etacount = 0; etacount<nEtaBins; ++etacount) std::cout<<etabins[etacount]<<std::endl;
  int npTbins = ((TList*)averageFits->At(0))->GetSize();
  std::cout<<"npTbins = "<<npTbins<<std::endl;
  TList* EtaDependencePlots = new TList();
  for(unsigned int i=0; i<npTbins; ++i){ //Defining a histogram for each pT bin
    TString name("etaDep_pT_"+TString::Format("%i",i));
    std::cout<<"name"<<name<<std::endl;
    float* auxEtaArray = &etabins[0];
    TH1F* pTibin = new TH1F(name,"mu(NPV) term dependence on eta for ipTbin",nEtaBins,&etabins[0]);
    //TH1F* pTibin = new TH1F(name,"mu(NPV) term dependence on eta for ipTbin",nEtaBins,auxEtaArray);
    //    TH1F* pTibin = new TH1F(name,"mu(NPV) term dependence on eta for ipTbin",nEtaBins,&EtaBinsArray[0]);
    EtaDependencePlots->Add(pTibin);
  }
  std::cout<<"Histos defined"<<std::endl;
  for(unsigned int j=0; j<nEtaBins;++j){
    //Here a loop on pt to fill each histogram with the parameter [0]
    for(unsigned int k=0; k<npTbins; ++k){
      double averageSlope = ((TF1*)((TList*)averageFits->At(j))->At(k))->GetParameter(0);
      double averageSlopeError = ((TF1*)((TList*)averageFits->At(j))->At(k))->GetParError(0);
      if(averageSlope && averageSlope!=-999.9){
	((TH1F*) EtaDependencePlots->At(k))->SetBinContent(j+1,averageSlope);
	((TH1F*) EtaDependencePlots->At(k))->SetBinError(j+1,averageSlopeError);
      }
    }
    
  }
  
  return EtaDependencePlots;
}



TList* PlotTGraphsDeltaPtvsPtAfterArea(TFile* fileTH3s, int npTbins){  
  //TList that will contain TList results
  TList* TGraphsLists = new TList();
  //TList for pTtrue
  TList* pTtrueGraphs = new TList();
  //TList for pTuncalib
  TList* pTuncalibGraphs = new TList();
  //Reading TList's of TH3's from input file
  TList * pTdiffHistList = (TList*) fileTH3s->Get("DeltaPtHistList"); //I will need to change the name
  if (!pTdiffHistList){
    std::cout <<"TList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  TList * pTtrueHistList = (TList*) fileTH3s->Get("pTtrueHistList");
  if (!pTtrueHistList){
    std::cout <<"pTtrueHistList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  TList * pTuncalibHistList = (TList*) fileTH3s->Get("pTuncalibHistList");
  if (!pTuncalibHistList){
    std::cout <<"pTuncalibHistList not found, exiting" << std::endl;
    return TGraphsLists;
  }
  //Number of bins
  int nhistograms = pTtrueHistList->GetSize();
  int nxbins = ((TH2F*) pTtrueHistList->At(0))->GetXaxis()->GetNbins();
  int nybins = ((TH2F*) pTtrueHistList->At(0))->GetYaxis()->GetNbins();
  int nEtabins = nhistograms/npTbins;
  if (nhistograms%npTbins != 0){
    std::cout<<"Incorrect number of pT bins, exiting"<<std::endl;
    return TGraphsLists;
  }

  for(int etacount = 0; etacount<nEtabins; ++etacount){ //Looping on eta
    for(int xbin = 0; xbin< nxbins; ++xbin){ //Looping on mu
      for(int ybin = 0; ybin < nybins; ++ybin){ //Looping on NPV
	std::vector<float> mean_pTarea_true;
	std::vector<float> mean_pTtrue;
	std::vector<float> mean_pTuncalib;
	std::vector<float> errX;
	std::vector<float> errY;
	TString name("eta_"+TString::Format("%i",etacount)+"_mu_"+TString::Format("%i",xbin)+"_NPV_"+TString::Format("%i",ybin));
	for(int ptcount = 0; ptcount < npTbins; ++ptcount){ //Loop on pT
	  //Reading bin content from z axis 
	  //	    std::cout<<"Histo:"<<((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
	    ((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xbin+1,xbin+1);
	    ((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ybin+1,ybin+1);
	  if (((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetEffectiveEntries()>100){

	    //	    std::cout<<"mu bin : "<<xbin<<",  NPV bin : "<<ybin<<std::endl;
	    //	    std::cout<<"Histo:"<<((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
	    mean_pTarea_true.push_back(((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1));
	    errY.push_back(((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetBinError(xbin+1,ybin+1));
	    //	    std::cout<<"pTarea-pTtrue = "<<((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1)<<std::endl;

	    ((TH2F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xbin+1,xbin+1);
	    ((TH2F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ybin+1,ybin+1);
	    //	    std::cout<<"pT true histo : "<<((TH2F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
	    mean_pTtrue.push_back(((TH2F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1));
	    //	    std::cout<<"pTtrue = "<<((TH2F*) pTtrueHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1)<<std::endl;

	    ((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xbin+1,xbin+1);
	    ((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ybin+1,ybin+1);
	    //	    std::cout<<"pT area histo : "<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
	    mean_pTuncalib.push_back(((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1));
	    errX.push_back(((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinError(xbin+1,ybin+1));
	    //	    std::cout<<"pTarea = "<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1)<<std::endl;
	  }
	  //	  if(xbin == 1){ std::cout<<"Eff entries : "<<((TH2F*) pTdiffHistList->At(etacount*npTbins+ptcount))->GetEffectiveEntries()<<std::endl;} //for debugging
	}

	int npoints = mean_pTtrue.size();
	//	mean_pTuncalib.push_back(300.0);
	//	mean_pTarea_true.push_back(NULL);
	TGraph* pTtrueGraph = new TGraph(npoints, &mean_pTtrue[0], &mean_pTarea_true[0]);
	pTtrueGraph->SetName("pTtrue"+name);
	TGraphErrors* pTuncalibGraph = new TGraphErrors(npoints, &mean_pTuncalib[0], &mean_pTarea_true[0],&errX[0],&errY[0]); 
	pTuncalibGraph->SetName("pTuncalib"+name);
	pTtrueGraphs->Add(pTtrueGraph);
	pTuncalibGraphs->Add(pTuncalibGraph);
	mean_pTarea_true.clear();
	mean_pTtrue.clear();
	mean_pTuncalib.clear();
      }
    }
  }
  TGraphsLists->Add(pTtrueGraphs);
  TGraphsLists->Add(pTuncalibGraphs);
  return TGraphsLists;
}
///////// work here ////////////////////////////////////////////////////////////////////
struct MeansAndErrors{
  std::vector<float> means;
  std::vector<float> means_err;
};

TList* PlotTGraphsDeltaPtAreavsPtAfterArea(TFile* fileTH3s, int npTbins, MeansAndErrors pTarea_means){  
  //TList that will contain TList results
  TList* pTareaGraphs = new TList();

  //Reading TList's of TH3's from input file
  TList * pTuncalibHistList = (TList*) fileTH3s->Get("pTuncalibHistList");
  if (!pTuncalibHistList){
    std::cout <<"pTuncalibHistList not found, exiting" << std::endl;
    return pTareaGraphs;
  }

  //Number of bins
  int nhistograms = pTuncalibHistList->GetSize();
  int nxbins = ((TH2F*) pTuncalibHistList->At(0))->GetXaxis()->GetNbins();
  int nybins = ((TH2F*) pTuncalibHistList->At(0))->GetYaxis()->GetNbins();
  int nEtabins = nhistograms/npTbins;
  if (nhistograms%npTbins != 0){
    std::cout<<"Incorrect number of pT bins, exiting"<<std::endl;
    return pTareaGraphs;
  }

  for(int etacount = 0; etacount<nEtabins; ++etacount){ //Looping on eta
    for(int xbin = 0; xbin< nxbins; ++xbin){ //Looping on mu
      for(int ybin = 0; ybin < nybins; ++ybin){ //Looping on NPV
	std::vector<float> mean_deltapTarea;
	std::vector<float> mean_pTarea;
	std::vector<float> errX;
	std::vector<float> errY;
	TString name("eta_"+TString::Format("%i",etacount)+"_mu_"+TString::Format("%i",xbin)+"_NPV_"+TString::Format("%i",ybin));
	for(int ptcount = 0; ptcount < npTbins; ++ptcount){ //Loop on pT
	  //Reading bin content from z axis 
	  //	    std::cout<<"Histo:"<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
	    ((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetXaxis()->SetRange(xbin+1,xbin+1);
	    ((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetYaxis()->SetRange(ybin+1,ybin+1);
	  if (((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetEffectiveEntries()>100){

	    //	    std::cout<<"mu bin : "<<xbin<<",  NPV bin : "<<ybin<<std::endl;
	    //	    std::cout<<"Histo:"<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetName()<<std::endl;
	    mean_deltapTarea.push_back(((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1) - pTarea_means.means[etacount*npTbins+ptcount]);
	    errY.push_back(((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinError(xbin+1,ybin+1)); //need to compute error properly <--------------------------------------
	    //	    std::cout<<"pTarea-pTtrue = "<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1)<<std::endl;

	    mean_pTarea.push_back(((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1));
	    errX.push_back(((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinError(xbin+1,ybin+1));
	    //	    std::cout<<"pTarea = "<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetBinContent(xbin+1,ybin+1)<<std::endl;
	  }
	  //	  if(xbin == 1){ std::cout<<"Eff entries : "<<((TH2F*) pTuncalibHistList->At(etacount*npTbins+ptcount))->GetEffectiveEntries()<<std::endl;} //for debugging
	}

	int npoints = mean_deltapTarea.size();
	//	mean_pTarea.push_back(300.0);
	//	mean_deltapTarea.push_back(NULL);
	TGraphErrors* pTuncalibGraph = new TGraphErrors(npoints, &mean_pTarea[0], &mean_deltapTarea[0],&errX[0],&errY[0]); 
	pTuncalibGraph->SetName("pTuncalib"+name);
	pTareaGraphs->Add(pTuncalibGraph);
	mean_deltapTarea.clear();
	mean_pTarea.clear();
      }
    }
  }
  return pTareaGraphs;
}