#define NEWRESIDUAL_H
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>

#include "TTree.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TList.h"
#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TChain.h"
#include "TF2.h"
#include "TH3F.h"
#include "TEnv.h"

#include "macros/ResidualTools.h"
#include "bcidtools.h"
#include "macros/CalibTools.h"

void Histos3D(string listoffiles, string inputconfig, string areaoption, string weightoption, int nEvents=-1, bool doBCID=false){

// Get input file (could be a list of input files as well)
  TString InputFile  (listoffiles);
  std::cout<<"InputFile: "<<InputFile<<std::endl;
  
  // Open config
  TString configName (inputconfig);
  TEnv* config = new TEnv(configName);
  if(!config){
    std::cout << configName << " not found, exiting" << std::endl;
    //return 0;
  }
  config->Print();
 
  // Get TTree
  TString TreeName = config->GetValue("TreeName","treeName");
  TTree* Tree = NULL;
  TChain* ch  = NULL;
  TFile* file = NULL;
  bool useTChain = false;

  //Read input and create TChain 
  if(InputFile.Contains(".root")){// single ROOT file provided
    // Open input ROOT file
    file = TFile::Open(InputFile);
    if(!file){
      std::cout << InputFile << " not found, exiting" << std::endl;
      //return 0;
    }
    // Get TTree
    Tree = (TTree*)file->Get(TreeName);
    std::cout << "Entries: " << Tree->GetEntries() << std::endl;
  } else if(InputFile.Contains(".txt")){ // list of ROOT files provided in a .txt file 
    useTChain = true;
    // Read list of input files
    std::ifstream in;
    std::cout << "Reading list of ROOT files from: " << InputFile << std::endl;
    in.open(InputFile.Data(),std::ifstream::in);
    TString name;
    std::vector<TString> names;
    // Reading root file names
    while (in >> name) {
      names.push_back(name);
    }
    in.close();
    // Create TChain
    ch = new TChain(TreeName);     //creates a chain to process a Tree called "TreeName"
    // Loop over root files
    for(unsigned int ifile=0;ifile<names.size();++ifile){
      std::cout << "Adding " << names.at(ifile) << " to the TChain" << std::endl;
      ch->Add(names.at(ifile));
    }
    std::cout << "Entries: " << ch->GetEntries() << std::endl;
  } else cout<<"cannot read input"<<endl;

  //Delta pt  binning
  int pTresidualnbins = config->GetValue("Delta_pTNBin",100); std::cout<<"pTresidualnbins: "<<pTresidualnbins<<std::endl;
  float pTresiduallow = config->GetValue("Delta_pTLow",0.);std::cout<<"pTresiduallow: "<<pTresiduallow<<std::endl;
  float pTresidualhigh = config->GetValue("Delta_pTHigh",100.);std::cout<<"pTresidualhigh: "<<pTresidualhigh<<std::endl;
  //NPV, mu binning
  int NPVnbins = config->GetValue("NPVNBin",25);std::cout<<"NPVnbins: "<<NPVnbins<<std::endl;
  float NPVlow = config->GetValue("NPVLow", 10.5);std::cout<<"NPVlow: "<<NPVlow<<std::endl;
  float NPVhigh = config->GetValue("NPVHigh",34.5);std::cout<<"NPVhigh: "<<NPVhigh<<std::endl;
  int munbins = config->GetValue("muNBin",17);std::cout<<"munbins: "<<munbins<<std::endl;
  float mulow = config->GetValue("muLow", 6.5);std::cout<<"mulow: "<<mulow<<std::endl;
  float muhigh = config->GetValue("muHigh",23.5);std::cout<<"muhigh: "<<muhigh<<std::endl;


//Defining binning for pt and eta true
  vector<float> ptbins, etabins; 
  TString ptvariable("TruePtBin"); //Later change it to read from config
  TString etavariable("TrueEtaBin"); //Later change it to read from config
  int nptbins = config->GetValue("TruePtNBin",10);
  int netabins = config->GetValue("TrueEtaNBin",10);
  cout<<"Number of pt bins:"<<nptbins<<endl;
  cout<<"Number of eta bins:"<<netabins<<endl;
  ptbins=ReadBinning(config,ptvariable,nptbins); //Reading pt binning from config
  etabins=ReadBinning(config,etavariable,netabins);//Reading eta binning from config
  cout<<"Binning read"<<endl;

  //Defining histograms for each (eta,pt) bin for pt-true pt-uncalibrated and (ptArea - pTtrue)
  //pTArea - pTtrue
  TList *l = new TList();
  TH3F* arrayh3d[1000] ={NULL}; //Be carefull with the overflow if more than 1000 histos needed !!!!!
  //  DefineHistos(netabins,nptbins,arrayh3d,l,NPVnbins,NPVlow,NPVhigh,munbins,mulow,muhigh,pTresidualnbins,pTresiduallow,pTresidualhigh);
  DefineHistos(netabins,nptbins,arrayh3d,l,munbins,mulow,muhigh,NPVnbins,NPVlow,NPVhigh,pTresidualnbins,pTresiduallow,pTresidualhigh,"diff_");
  //pT-true
  TList *pTtrueHistList = new TList();
  TH3F* pTtrueHistArray[1000] = {NULL};
  DefineHistos(netabins, nptbins, pTtrueHistArray, pTtrueHistList, munbins,mulow,muhigh,NPVnbins,NPVlow,NPVhigh, pTresidualnbins, pTresiduallow,pTresidualhigh, "true_");
  //pT-uncalibrated
  TList *pTuncalibHistList = new TList();
  TH3F* pTuncalibHistArray[1000] = {NULL};
  DefineHistos(netabins, nptbins, pTuncalibHistArray, pTuncalibHistList, munbins,mulow,muhigh,NPVnbins,NPVlow,NPVhigh, pTresidualnbins, pTresiduallow, pTresidualhigh, "uncalib_");
  
  //Defining histograms to check some distribution
  TH1F* pTtrue_weight = new TH1F("pTtrue_weight","pTtrue weight considered",200,0.,200.);
  TH1F* pTtrue = new TH1F("pTtrue","pTtrue weight not considered",200,0.,200.);
  TH1F* mu_dist = new TH1F("mu","mu",20,10.,70.);
  TH1I* NPV_dist = new TH1I("NPV","NPV",20,5,45);

  //Reading name of branches from config file
  vector <TString> branches;
  branches = ReadBranches(config);

  //Creating file for results
  TString ResidualVersion = config->GetValue("Version","VERSION");
  TString JetAlgo = config->GetValue("JetAlgos","");

  vector<float>* pt = NULL; //true pt
  vector<float>* recojet_pt = NULL; //const scale pt
  Int_t           NPV = 0;
  Float_t         mu = 0;
  vector<float>*  jet_area = NULL; //area
  Double_t        rho = 0;
  vector<float>* true_eta = NULL;
  //  Float_t         weight=0;
  Double_t        weight=0;
  std::vector<float>* recojet_eta = NULL; //const scale eta
  Double_t AreaCorr=0.0;
  Int_t bcid = 0;
  Int_t bcid2 = 0;

  float edgesMC16de[]={82,216,405,594,787,921,1110,1299,1488,1681,1815,2004,2193,2382,2575,2709,2898,3087,3276};
  TList* l_bcid = DefinePtEtaHistos(netabins,nptbins,216,0,216,pTresidualnbins,pTresiduallow,pTresidualhigh);
  //  Declare_variables(pt,recojet_pt,NPV,mu,rho,jet_area,reco_eta,ch,branches);

  ch->SetBranchAddress(branches[0], &pt);
  ch->SetBranchAddress(branches[1], &recojet_pt);
  ch->SetBranchAddress(branches[2], &NPV);
  ch->SetBranchAddress(branches[3], &mu);
  ch->SetBranchAddress(branches[4], &rho);
  ch->SetBranchAddress(branches[5], &jet_area);
  ch->SetBranchAddress(branches[6], &true_eta);
  ch->SetBranchAddress(branches[7], &weight);
  ch->SetBranchAddress(branches[8], &recojet_eta);
  ch->SetBranchAddress(branches[9], &bcid);

  cout<<"branch set"<<std::endl;

  //Reading options for weight and area substraction
  TString AreaSub  (areaoption);
  std::cout<<"Area Subtraction option: "<<AreaSub<<std::endl;
  TString ApplyWeight  (weightoption);
  std::cout<<"Apply weight option: "<<ApplyWeight<<std::endl;

  for(int ientry=0;ientry<ch->GetEntries();ientry++){ //Loop on entries
    ch->GetEntry(ientry);
    mu_dist->Fill(mu,weight);
    NPV_dist->Fill(NPV,weight);
    for(unsigned int j=0;j<pt->size();j++){ //Loop on jets of each entry
      pTtrue_weight->Fill(pt->at(j),weight);
      pTtrue->Fill(pt->at(j));
      AreaCorr = 0.0;
      //BCID variable
      bcid2 = GetBCID2(bcid, edgesMC16de);
      for(unsigned int etacount=0; etacount<(etabins.size()-1);++etacount){ //Loop on eta bins
	if(abs(true_eta->at(j))>=etabins[etacount] && abs(true_eta->at(j))<etabins[etacount+1]){
	  for(unsigned int ptcount=0;ptcount<(ptbins.size()-1);++ptcount){ //Loop on pt bins
	    if(pt->at(j)>=ptbins[ptcount] && pt->at(j)<ptbins[ptcount+1]){ 
	      AreaCorr = jet_area->at(j)*rho*0.001;
	      if(AreaSub == "NoArea") AreaCorr = 0.0;
        else if(AreaSub == "NoForwardArea" && abs(true_eta->at(j))>=2.5) AreaCorr = 0.0;

		  if(ApplyWeight == "ApplyWeight"){
		    arrayh3d[etacount*(ptbins.size()-1)+ptcount]->Fill(mu,NPV,recojet_pt->at(j)-pt->at(j)-AreaCorr,weight); 
        pTtrueHistArray[etacount*(ptbins.size()-1)+ptcount]->Fill(mu,NPV,pt->at(j),weight); 
		    pTuncalibHistArray[etacount*(ptbins.size()-1)+ptcount]->Fill(mu,NPV,recojet_pt->at(j)-AreaCorr,weight); 
		  ((TH2D*)l_bcid->At(etacount*nptbins+ptcount))->Fill(bcid2,recojet_pt->at(j)-pt->at(j)-AreaCorr,weight);

		  }
		  else if(ApplyWeight == "NoWeight"){
		    arrayh3d[etacount*(ptbins.size()-1)+ptcount]->Fill(mu,NPV,recojet_pt->at(j)-pt->at(j)-AreaCorr); 
        pTtrueHistArray[etacount*(ptbins.size()-1)+ptcount]->Fill(mu,NPV,pt->at(j)); 
		    pTuncalibHistArray[etacount*(ptbins.size()-1)+ptcount]->Fill(mu,NPV,recojet_pt->at(j)-AreaCorr);
		  ((TH2D*)l_bcid->At(etacount*nptbins+ptcount))->Fill(bcid2,recojet_pt->at(j)-pt->at(j)-AreaCorr);

		  }	  
      //std::cout<<"abs(eta) = "<<abs(true_eta->at(j))<<std::endl;
      //std::cout<<"AreaCorr = "<<AreaCorr<<std::endl;
	      
	    }//close condition on pt
	  } //close loop on pt bins
	} //close if eta
      } //close loop on eta bins
    } //close loop on jets
    if (ientry%1000000==0) std::cout<<"Entry: "<<ientry<<std::endl;
    if (nEvents>0 && ientry==nEvents ) break;
  } //close loop on entries

  //  TH1D* MeanDeltaPtBCID = PTools::GetMean(DeltaPtBCID);
  TList* l_MeanDeltaPtBCID = NULL;
  if (doBCID) l_MeanDeltaPtBCID = PTools::GetMean(l_bcid,1);
  arrayh3d[0]->Draw();
  system("mkdir TH3histograms");
  TString name("./TH3histograms/TH3_"+ResidualVersion+JetAlgo+".root");
  //  TFile result("/TH3histograms/"+name,"RECREATE");
  TFile result(name,"RECREATE");
  l->Write("DeltaPtHistList",TObject::kSingleKey); //pTarea - pTuncalib
  pTtrueHistList->Write("pTtrueHistList",TObject::kSingleKey); //pT-true
  pTuncalibHistList->Write("pTuncalibHistList",TObject::kSingleKey); //pTarea - pTuncalib
  //l->Write();
  if(doBCID){
    l_bcid->Write("BCID",TObject::kSingleKey);
    l_MeanDeltaPtBCID->Write("MeanDeltaPtBCID",TObject::kSingleKey);
  }
  pTtrue->Write();
  pTtrue_weight->Write();
  mu_dist->Write();
  NPV_dist->Write();
  cout<<"histograms written"<<endl;
  result.Close();
}

